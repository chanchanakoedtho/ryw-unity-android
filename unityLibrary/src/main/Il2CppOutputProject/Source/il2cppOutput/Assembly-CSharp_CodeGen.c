﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void CharacterRotate::Start()
extern void CharacterRotate_Start_m5C3A32BDAB1E800791F0A79627E15062D9776C5D (void);
// 0x00000002 System.Void CharacterRotate::Update()
extern void CharacterRotate_Update_mA2295A96DDDBC839DE76C863701DF2D0AAE481C5 (void);
// 0x00000003 System.Void CharacterRotate::.ctor()
extern void CharacterRotate__ctor_m96B237055E719DC8DEA6E7BBAADCFE2C240BD6C4 (void);
// 0x00000004 System.Void CusTomsCharacter::Start()
extern void CusTomsCharacter_Start_m1126B9F99F522F16DEADE8B558CE58834CCDADCF (void);
// 0x00000005 System.Void CusTomsCharacter::CustomUpdate()
extern void CusTomsCharacter_CustomUpdate_mABF82C935643D4243E52751A6C310EED32CE279D (void);
// 0x00000006 System.Void CusTomsCharacter::UpdateModel()
extern void CusTomsCharacter_UpdateModel_mAB1ED5E45E097B9B4DFED4580ACE63DC01598095 (void);
// 0x00000007 System.Void CusTomsCharacter::ChangeBar(System.Int32)
extern void CusTomsCharacter_ChangeBar_mB9EB2CA8C00323C8D14F41D95B7EEF55BF033C77 (void);
// 0x00000008 System.Void CusTomsCharacter::ChangPart(System.Int32)
extern void CusTomsCharacter_ChangPart_m16EA37DB4411419A675C34159FC05CADE93591DA (void);
// 0x00000009 System.Void CusTomsCharacter::UpdateWithJson(System.String)
extern void CusTomsCharacter_UpdateWithJson_mD5D1E3F428BB38704DF94C78C80FF6EFD07CA228 (void);
// 0x0000000A System.Void CusTomsCharacter::UpdateFromNative(System.String)
extern void CusTomsCharacter_UpdateFromNative_mEF7D32CFB4E2155A68E195DB0A8299767B256384 (void);
// 0x0000000B System.Void CusTomsCharacter::SetMenuBar()
extern void CusTomsCharacter_SetMenuBar_m712FE3131208BC62D1527F5EF06A2D922231D741 (void);
// 0x0000000C System.Void CusTomsCharacter::UpdateIgnoreNative(System.String)
extern void CusTomsCharacter_UpdateIgnoreNative_mB5DA83D13368EB724DBE601D03F224873CE96562 (void);
// 0x0000000D System.Void CusTomsCharacter::UpdateOneItemFromNative(System.String)
extern void CusTomsCharacter_UpdateOneItemFromNative_mB973398E0DDCF521D10D4AB02ACD5A1FF11A2EAC (void);
// 0x0000000E System.Void CusTomsCharacter::ResetCharacter()
extern void CusTomsCharacter_ResetCharacter_m6AE0EC86DC08D952D38B6B37AABC1888911A73E9 (void);
// 0x0000000F System.Void CusTomsCharacter::.ctor()
extern void CusTomsCharacter__ctor_mFF497519CBCA0F5AB1DECD378624F568ED02C47F (void);
// 0x00000010 System.Void ExtensionCusTomsCharacter::UpdateItemToNative(CusTomsCharacter)
extern void ExtensionCusTomsCharacter_UpdateItemToNative_mADD4924A4A10948BC4CAF637DC30ACFBCA95C756 (void);
// 0x00000011 System.Void ExtensionCusTomsCharacter::LastSelectItem(System.Int32)
extern void ExtensionCusTomsCharacter_LastSelectItem_mD147F8F18FEC6C1E58A810A4E1DD76FA9CFCBCE1 (void);
// 0x00000012 CharacterLastItem ExtensionCusTomsCharacter::ConvertToCharacterLastItem(System.Int32)
extern void ExtensionCusTomsCharacter_ConvertToCharacterLastItem_mC446324403D52ECFDB3430A23A2022A4E0640FA9 (void);
// 0x00000013 CharacterLastItem ExtensionCusTomsCharacter::MakeCharacterLastItem(System.String,System.Int32)
extern void ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C (void);
// 0x00000014 System.Int32 ExtensionCusTomsCharacter::ToInt(System.Boolean)
extern void ExtensionCusTomsCharacter_ToInt_m2554F281D955C23A229781EBF24BFEB2410F3FDA (void);
// 0x00000015 System.Void ExtensionCusTomsCharacter::ResetPositionCharacter(UnityEngine.Transform,System.Single)
extern void ExtensionCusTomsCharacter_ResetPositionCharacter_mF8AA537F5FF98FC57700354E2F3DABFDA635DB9B (void);
// 0x00000016 System.Void ExtensionCusTomsCharacter::StartScene(CusTomsCharacter)
extern void ExtensionCusTomsCharacter_StartScene_mC0988804556E30B98C5D352E867E7F601F3E892B (void);
// 0x00000017 System.Void ExtensionCusTomsCharacter/NativeAPI::CallAndroidMethod(System.String,System.Object)
extern void NativeAPI_CallAndroidMethod_m309542DA3F713FB85889EFEFF13A15580E579A83 (void);
// 0x00000018 System.Void ExtensionCusTomsCharacter/NativeAPI::SendToNative(System.String)
extern void NativeAPI_SendToNative_mE1A3D6410681C7761C6F163B61D1B731995B79F1 (void);
// 0x00000019 System.Void ExtensionCusTomsCharacter/NativeAPI::LastSelectItem(System.String)
extern void NativeAPI_LastSelectItem_mE97F9CE03B9769E54869571EC1AD9A8CAEAC6B68 (void);
// 0x0000001A System.Void ExtensionCusTomsCharacter/NativeAPI::StartScene()
extern void NativeAPI_StartScene_m213C8305BFD872BC5ECB55E49ACF01DA1F88C15E (void);
// 0x0000001B System.Void ExtensionCusTomsCharacter/NativeAPI::.ctor()
extern void NativeAPI__ctor_m93471F43776EABE22E0746CF1E98CD3ECD13E0ED (void);
// 0x0000001C System.Void ModelDetails::Start()
extern void ModelDetails_Start_m6FB658FB5B4FD02CCF117C740F7120E0267F221D (void);
// 0x0000001D System.Void ModelDetails::Update()
extern void ModelDetails_Update_mCB2B959309C64D086C8B486942DA9A29BF0662F0 (void);
// 0x0000001E System.Void ModelDetails::.ctor()
extern void ModelDetails__ctor_m118EF6DBD042861F08936137E6FD167991F85415 (void);
// 0x0000001F System.Void UISpriteSwap::Start()
extern void UISpriteSwap_Start_mFBECA1CCCC144E1C13A7BF1A863DDD950D733952 (void);
// 0x00000020 System.Void UISpriteSwap::Update()
extern void UISpriteSwap_Update_mF5A7E5C1898FFC2D1EC55D6E778F2ED5EFD0EB17 (void);
// 0x00000021 System.Void UISpriteSwap::.ctor()
extern void UISpriteSwap__ctor_mB92DDC7D233A7F19CAE938F736DC2DD1CC16E9BB (void);
static Il2CppMethodPointer s_methodPointers[33] = 
{
	CharacterRotate_Start_m5C3A32BDAB1E800791F0A79627E15062D9776C5D,
	CharacterRotate_Update_mA2295A96DDDBC839DE76C863701DF2D0AAE481C5,
	CharacterRotate__ctor_m96B237055E719DC8DEA6E7BBAADCFE2C240BD6C4,
	CusTomsCharacter_Start_m1126B9F99F522F16DEADE8B558CE58834CCDADCF,
	CusTomsCharacter_CustomUpdate_mABF82C935643D4243E52751A6C310EED32CE279D,
	CusTomsCharacter_UpdateModel_mAB1ED5E45E097B9B4DFED4580ACE63DC01598095,
	CusTomsCharacter_ChangeBar_mB9EB2CA8C00323C8D14F41D95B7EEF55BF033C77,
	CusTomsCharacter_ChangPart_m16EA37DB4411419A675C34159FC05CADE93591DA,
	CusTomsCharacter_UpdateWithJson_mD5D1E3F428BB38704DF94C78C80FF6EFD07CA228,
	CusTomsCharacter_UpdateFromNative_mEF7D32CFB4E2155A68E195DB0A8299767B256384,
	CusTomsCharacter_SetMenuBar_m712FE3131208BC62D1527F5EF06A2D922231D741,
	CusTomsCharacter_UpdateIgnoreNative_mB5DA83D13368EB724DBE601D03F224873CE96562,
	CusTomsCharacter_UpdateOneItemFromNative_mB973398E0DDCF521D10D4AB02ACD5A1FF11A2EAC,
	CusTomsCharacter_ResetCharacter_m6AE0EC86DC08D952D38B6B37AABC1888911A73E9,
	CusTomsCharacter__ctor_mFF497519CBCA0F5AB1DECD378624F568ED02C47F,
	ExtensionCusTomsCharacter_UpdateItemToNative_mADD4924A4A10948BC4CAF637DC30ACFBCA95C756,
	ExtensionCusTomsCharacter_LastSelectItem_mD147F8F18FEC6C1E58A810A4E1DD76FA9CFCBCE1,
	ExtensionCusTomsCharacter_ConvertToCharacterLastItem_mC446324403D52ECFDB3430A23A2022A4E0640FA9,
	ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C,
	ExtensionCusTomsCharacter_ToInt_m2554F281D955C23A229781EBF24BFEB2410F3FDA,
	ExtensionCusTomsCharacter_ResetPositionCharacter_mF8AA537F5FF98FC57700354E2F3DABFDA635DB9B,
	ExtensionCusTomsCharacter_StartScene_mC0988804556E30B98C5D352E867E7F601F3E892B,
	NativeAPI_CallAndroidMethod_m309542DA3F713FB85889EFEFF13A15580E579A83,
	NativeAPI_SendToNative_mE1A3D6410681C7761C6F163B61D1B731995B79F1,
	NativeAPI_LastSelectItem_mE97F9CE03B9769E54869571EC1AD9A8CAEAC6B68,
	NativeAPI_StartScene_m213C8305BFD872BC5ECB55E49ACF01DA1F88C15E,
	NativeAPI__ctor_m93471F43776EABE22E0746CF1E98CD3ECD13E0ED,
	ModelDetails_Start_m6FB658FB5B4FD02CCF117C740F7120E0267F221D,
	ModelDetails_Update_mCB2B959309C64D086C8B486942DA9A29BF0662F0,
	ModelDetails__ctor_m118EF6DBD042861F08936137E6FD167991F85415,
	UISpriteSwap_Start_mFBECA1CCCC144E1C13A7BF1A863DDD950D733952,
	UISpriteSwap_Update_mF5A7E5C1898FFC2D1EC55D6E778F2ED5EFD0EB17,
	UISpriteSwap__ctor_mB92DDC7D233A7F19CAE938F736DC2DD1CC16E9BB,
};
static const int32_t s_InvokerIndices[33] = 
{
	1122,
	1122,
	1122,
	1122,
	1122,
	1122,
	950,
	950,
	959,
	959,
	1122,
	959,
	959,
	1122,
	1122,
	1920,
	1917,
	1783,
	1589,
	1828,
	1769,
	1920,
	1766,
	1920,
	1920,
	1954,
	1122,
	1122,
	1122,
	1122,
	1122,
	1122,
	1122,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	33,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
