﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t815A476B0A21E183042059E705F9E505478CD8AE;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween>
struct TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4;
// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4;
// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D;
// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C;
// UnityEngine.Canvas
struct Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E;
// CharacterRotate
struct CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD;
// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684;
// CusTomsCharacter
struct CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.GlobalJavaObjectRef
struct GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.Mesh
struct Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6;
// ModelDetails
struct ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.UI.RectMask2D
struct RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// System.String
struct String_t;
// UnityEngine.Texture2D
struct Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// UISpriteSwap
struct UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883;
// UnityEngine.Events.UnityAction
struct UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099;
// UnityEngine.UI.VertexHelper
struct VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.UI.Button/ButtonClickedEvent
struct ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D;
// ExtensionCusTomsCharacter/NativeAPI
struct NativeAPI_tF772D52CA80B786271D41D2D49DA78F644213651;
// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent
struct CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4;

IL2CPP_EXTERN_C RuntimeClass* AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral227DA7A8EA45379E2CD7C2CE4658EBC0AC5EC9BE;
IL2CPP_EXTERN_C String_t* _stringLiteral2A557BCDD1470D7822A2296783B75E92CBDCB8A3;
IL2CPP_EXTERN_C String_t* _stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078;
IL2CPP_EXTERN_C String_t* _stringLiteral50863FA6986F7E76E712DFF2F2A2AD0E3E7AE14D;
IL2CPP_EXTERN_C String_t* _stringLiteral9B4EB089C2114281619D0CCE8FC65CE8F7A6C788;
IL2CPP_EXTERN_C String_t* _stringLiteralB8A41C05229F632972A5E5BD30A0C7CD675CD7F1;
IL2CPP_EXTERN_C String_t* _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709;
IL2CPP_EXTERN_C String_t* _stringLiteralDB21527FC9B6F09BC7BB132750502BD3A956B226;
IL2CPP_EXTERN_C String_t* _stringLiteralDDE2976751DA4F0E8E52A81CE7C26335F3B9304D;
IL2CPP_EXTERN_C String_t* _stringLiteralDE8D19E423C142F62412E91C74431E74DF9816EB;
IL2CPP_EXTERN_C String_t* _stringLiteralFAA454C5551F0BFB4AAE794589CC8CBD44FA5A4C;
IL2CPP_EXTERN_C String_t* _stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9;
IL2CPP_EXTERN_C const RuntimeMethod* AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_mC84C97A7EC20ED712D21107C9FA32E0785021153_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisCharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD_m4D544A881C6D200B0CFF1470B48069BF0CFEB20A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6_m9A3AC35D76078DA89CD45B502D62582166B20518_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* GameObject_GetComponent_TisUISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883_m46590BE8DCFBDA4B920ADF4F183ED1E84BABA64B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* JsonUtility_FromJson_TisCharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_mE6DE82CCF8F3E3A7EAB924FA577A0DB5A541FA59_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* JsonUtility_FromJson_TisCharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_mB4EEF589EB7B5E5D751D8DBEC8DE65025380131E_RuntimeMethod_var;

struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642;
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tFDCAFCBB4B3431CFF2DC4D3E03FBFDF54EFF7E9A 
{
public:

public:
};


// System.Object


// UnityEngine.AndroidJavaObject
struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E  : public RuntimeObject
{
public:
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jobject
	GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * ___m_jobject_1;
	// UnityEngine.GlobalJavaObjectRef UnityEngine.AndroidJavaObject::m_jclass
	GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * ___m_jclass_2;

public:
	inline static int32_t get_offset_of_m_jobject_1() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E, ___m_jobject_1)); }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * get_m_jobject_1() const { return ___m_jobject_1; }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 ** get_address_of_m_jobject_1() { return &___m_jobject_1; }
	inline void set_m_jobject_1(GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * value)
	{
		___m_jobject_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jobject_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_jclass_2() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E, ___m_jclass_2)); }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * get_m_jclass_2() const { return ___m_jclass_2; }
	inline GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 ** get_address_of_m_jclass_2() { return &___m_jclass_2; }
	inline void set_m_jclass_2(GlobalJavaObjectRef_t04A7D04EB0317C286F089E4DB4444EC4F2D78289 * value)
	{
		___m_jclass_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_jclass_2), (void*)value);
	}
};

struct AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_StaticFields
{
public:
	// System.Boolean UnityEngine.AndroidJavaObject::enableDebugPrints
	bool ___enableDebugPrints_0;

public:
	inline static int32_t get_offset_of_enableDebugPrints_0() { return static_cast<int32_t>(offsetof(AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_StaticFields, ___enableDebugPrints_0)); }
	inline bool get_enableDebugPrints_0() const { return ___enableDebugPrints_0; }
	inline bool* get_address_of_enableDebugPrints_0() { return &___enableDebugPrints_0; }
	inline void set_enableDebugPrints_0(bool value)
	{
		___enableDebugPrints_0 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// CharacterItemName
struct CharacterItemName_t43286C9FDCDE64C487E2EB781213DFD1D3B468F1  : public RuntimeObject
{
public:

public:
};


// ExtensionCusTomsCharacter
struct ExtensionCusTomsCharacter_t840E88E1734758D026B5B51CD4B479D06ADEF0FE  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// ExtensionCusTomsCharacter/NativeAPI
struct NativeAPI_tF772D52CA80B786271D41D2D49DA78F644213651  : public RuntimeObject
{
public:

public:
};


// UnityEngine.AndroidJavaClass
struct AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4  : public AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E
{
public:

public:
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// CharacterItem
struct CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1 
{
public:
	// System.Boolean CharacterItem::gender
	bool ___gender_0;
	// System.Int32 CharacterItem::hat
	int32_t ___hat_1;
	// System.Int32 CharacterItem::shirt
	int32_t ___shirt_2;
	// System.Int32 CharacterItem::pants
	int32_t ___pants_3;
	// System.Int32 CharacterItem::bag
	int32_t ___bag_4;
	// System.Int32 CharacterItem::accesories
	int32_t ___accesories_5;
	// System.Int32 CharacterItem::shoe
	int32_t ___shoe_6;

public:
	inline static int32_t get_offset_of_gender_0() { return static_cast<int32_t>(offsetof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1, ___gender_0)); }
	inline bool get_gender_0() const { return ___gender_0; }
	inline bool* get_address_of_gender_0() { return &___gender_0; }
	inline void set_gender_0(bool value)
	{
		___gender_0 = value;
	}

	inline static int32_t get_offset_of_hat_1() { return static_cast<int32_t>(offsetof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1, ___hat_1)); }
	inline int32_t get_hat_1() const { return ___hat_1; }
	inline int32_t* get_address_of_hat_1() { return &___hat_1; }
	inline void set_hat_1(int32_t value)
	{
		___hat_1 = value;
	}

	inline static int32_t get_offset_of_shirt_2() { return static_cast<int32_t>(offsetof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1, ___shirt_2)); }
	inline int32_t get_shirt_2() const { return ___shirt_2; }
	inline int32_t* get_address_of_shirt_2() { return &___shirt_2; }
	inline void set_shirt_2(int32_t value)
	{
		___shirt_2 = value;
	}

	inline static int32_t get_offset_of_pants_3() { return static_cast<int32_t>(offsetof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1, ___pants_3)); }
	inline int32_t get_pants_3() const { return ___pants_3; }
	inline int32_t* get_address_of_pants_3() { return &___pants_3; }
	inline void set_pants_3(int32_t value)
	{
		___pants_3 = value;
	}

	inline static int32_t get_offset_of_bag_4() { return static_cast<int32_t>(offsetof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1, ___bag_4)); }
	inline int32_t get_bag_4() const { return ___bag_4; }
	inline int32_t* get_address_of_bag_4() { return &___bag_4; }
	inline void set_bag_4(int32_t value)
	{
		___bag_4 = value;
	}

	inline static int32_t get_offset_of_accesories_5() { return static_cast<int32_t>(offsetof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1, ___accesories_5)); }
	inline int32_t get_accesories_5() const { return ___accesories_5; }
	inline int32_t* get_address_of_accesories_5() { return &___accesories_5; }
	inline void set_accesories_5(int32_t value)
	{
		___accesories_5 = value;
	}

	inline static int32_t get_offset_of_shoe_6() { return static_cast<int32_t>(offsetof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1, ___shoe_6)); }
	inline int32_t get_shoe_6() const { return ___shoe_6; }
	inline int32_t* get_address_of_shoe_6() { return &___shoe_6; }
	inline void set_shoe_6(int32_t value)
	{
		___shoe_6 = value;
	}
};

// Native definition for P/Invoke marshalling of CharacterItem
struct CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_pinvoke
{
	int32_t ___gender_0;
	int32_t ___hat_1;
	int32_t ___shirt_2;
	int32_t ___pants_3;
	int32_t ___bag_4;
	int32_t ___accesories_5;
	int32_t ___shoe_6;
};
// Native definition for COM marshalling of CharacterItem
struct CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_com
{
	int32_t ___gender_0;
	int32_t ___hat_1;
	int32_t ___shirt_2;
	int32_t ___pants_3;
	int32_t ___bag_4;
	int32_t ___accesories_5;
	int32_t ___shoe_6;
};

// CharacterLastItem
struct CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E 
{
public:
	// System.String CharacterLastItem::itemName
	String_t* ___itemName_0;
	// System.Boolean CharacterLastItem::isActive
	bool ___isActive_1;

public:
	inline static int32_t get_offset_of_itemName_0() { return static_cast<int32_t>(offsetof(CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E, ___itemName_0)); }
	inline String_t* get_itemName_0() const { return ___itemName_0; }
	inline String_t** get_address_of_itemName_0() { return &___itemName_0; }
	inline void set_itemName_0(String_t* value)
	{
		___itemName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___itemName_0), (void*)value);
	}

	inline static int32_t get_offset_of_isActive_1() { return static_cast<int32_t>(offsetof(CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E, ___isActive_1)); }
	inline bool get_isActive_1() const { return ___isActive_1; }
	inline bool* get_address_of_isActive_1() { return &___isActive_1; }
	inline void set_isActive_1(bool value)
	{
		___isActive_1 = value;
	}
};

// Native definition for P/Invoke marshalling of CharacterLastItem
struct CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_pinvoke
{
	char* ___itemName_0;
	int32_t ___isActive_1;
};
// Native definition for COM marshalling of CharacterLastItem
struct CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_com
{
	Il2CppChar* ___itemName_0;
	int32_t ___isActive_1;
};

// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// System.Single
struct Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tE07797BA3C98D4CA9B5A19413C19A76688AB899E, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// UnityEngine.Vector4
struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 
{
public:
	// System.Single UnityEngine.Vector4::x
	float ___x_1;
	// System.Single UnityEngine.Vector4::y
	float ___y_2;
	// System.Single UnityEngine.Vector4::z
	float ___z_3;
	// System.Single UnityEngine.Vector4::w
	float ___w_4;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}

	inline static int32_t get_offset_of_w_4() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7, ___w_4)); }
	inline float get_w_4() const { return ___w_4; }
	inline float* get_address_of_w_4() { return &___w_4; }
	inline void set_w_4(float value)
	{
		___w_4 = value;
	}
};

struct Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields
{
public:
	// UnityEngine.Vector4 UnityEngine.Vector4::zeroVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___zeroVector_5;
	// UnityEngine.Vector4 UnityEngine.Vector4::oneVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___oneVector_6;
	// UnityEngine.Vector4 UnityEngine.Vector4::positiveInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___positiveInfinityVector_7;
	// UnityEngine.Vector4 UnityEngine.Vector4::negativeInfinityVector
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___negativeInfinityVector_8;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___zeroVector_5)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___oneVector_6)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_7() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___positiveInfinityVector_7)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_positiveInfinityVector_7() const { return ___positiveInfinityVector_7; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_positiveInfinityVector_7() { return &___positiveInfinityVector_7; }
	inline void set_positiveInfinityVector_7(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___positiveInfinityVector_7 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7_StaticFields, ___negativeInfinityVector_8)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_negativeInfinityVector_8() const { return ___negativeInfinityVector_8; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_negativeInfinityVector_8() { return &___negativeInfinityVector_8; }
	inline void set_negativeInfinityVector_8(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___negativeInfinityVector_8 = value;
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Space
struct Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t568D704D2B0AAC3E5894DDFF13DB2E02E2CD539E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/FillMethod
struct FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2 
{
public:
	// System.Int32 UnityEngine.UI.Image/FillMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FillMethod_tC37E5898D113A8FBF25A6AB6FBA451CC51E211E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Image/Type
struct Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12 
{
public:
	// System.Int32 UnityEngine.UI.Image/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tDCB08AB7425CAB70C1E46CC341F877423B5A5E12, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Camera
struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};

struct Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_tD9E7B69E561CE2EFDEEDB0E7F1406AC52247160D * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// CharacterRotate
struct CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single CharacterRotate::SceneWidth
	float ___SceneWidth_4;
	// UnityEngine.Vector3 CharacterRotate::PressPoint
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___PressPoint_5;
	// UnityEngine.Quaternion CharacterRotate::StartRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___StartRotation_6;

public:
	inline static int32_t get_offset_of_SceneWidth_4() { return static_cast<int32_t>(offsetof(CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD, ___SceneWidth_4)); }
	inline float get_SceneWidth_4() const { return ___SceneWidth_4; }
	inline float* get_address_of_SceneWidth_4() { return &___SceneWidth_4; }
	inline void set_SceneWidth_4(float value)
	{
		___SceneWidth_4 = value;
	}

	inline static int32_t get_offset_of_PressPoint_5() { return static_cast<int32_t>(offsetof(CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD, ___PressPoint_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_PressPoint_5() const { return ___PressPoint_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_PressPoint_5() { return &___PressPoint_5; }
	inline void set_PressPoint_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___PressPoint_5 = value;
	}

	inline static int32_t get_offset_of_StartRotation_6() { return static_cast<int32_t>(offsetof(CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD, ___StartRotation_6)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_StartRotation_6() const { return ___StartRotation_6; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_StartRotation_6() { return &___StartRotation_6; }
	inline void set_StartRotation_6(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___StartRotation_6 = value;
	}
};


// CusTomsCharacter
struct CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] CusTomsCharacter::MainBar
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___MainBar_4;
	// UnityEngine.GameObject[] CusTomsCharacter::Bar
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___Bar_5;
	// UnityEngine.GameObject CusTomsCharacter::MainBarPlane
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___MainBarPlane_6;
	// UnityEngine.GameObject CusTomsCharacter::BarPlane
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___BarPlane_7;
	// UnityEngine.GameObject CusTomsCharacter::GenderPlane
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___GenderPlane_8;
	// UnityEngine.GameObject CusTomsCharacter::ModelGirle
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ModelGirle_9;
	// UnityEngine.GameObject CusTomsCharacter::ModelMan
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___ModelMan_10;
	// CharacterRotate CusTomsCharacter::ModelRotate
	CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * ___ModelRotate_11;
	// System.Boolean CusTomsCharacter::Gender
	bool ___Gender_12;
	// System.Int32 CusTomsCharacter::Het
	int32_t ___Het_13;
	// System.Int32 CusTomsCharacter::Shirt
	int32_t ___Shirt_14;
	// System.Int32 CusTomsCharacter::Pants
	int32_t ___Pants_15;
	// System.Int32 CusTomsCharacter::Bag
	int32_t ___Bag_16;
	// System.Int32 CusTomsCharacter::Accesories
	int32_t ___Accesories_17;
	// System.Int32 CusTomsCharacter::Shoe
	int32_t ___Shoe_18;

public:
	inline static int32_t get_offset_of_MainBar_4() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___MainBar_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_MainBar_4() const { return ___MainBar_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_MainBar_4() { return &___MainBar_4; }
	inline void set_MainBar_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___MainBar_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainBar_4), (void*)value);
	}

	inline static int32_t get_offset_of_Bar_5() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Bar_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_Bar_5() const { return ___Bar_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_Bar_5() { return &___Bar_5; }
	inline void set_Bar_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___Bar_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Bar_5), (void*)value);
	}

	inline static int32_t get_offset_of_MainBarPlane_6() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___MainBarPlane_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_MainBarPlane_6() const { return ___MainBarPlane_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_MainBarPlane_6() { return &___MainBarPlane_6; }
	inline void set_MainBarPlane_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___MainBarPlane_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainBarPlane_6), (void*)value);
	}

	inline static int32_t get_offset_of_BarPlane_7() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___BarPlane_7)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_BarPlane_7() const { return ___BarPlane_7; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_BarPlane_7() { return &___BarPlane_7; }
	inline void set_BarPlane_7(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___BarPlane_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___BarPlane_7), (void*)value);
	}

	inline static int32_t get_offset_of_GenderPlane_8() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___GenderPlane_8)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_GenderPlane_8() const { return ___GenderPlane_8; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_GenderPlane_8() { return &___GenderPlane_8; }
	inline void set_GenderPlane_8(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___GenderPlane_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GenderPlane_8), (void*)value);
	}

	inline static int32_t get_offset_of_ModelGirle_9() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___ModelGirle_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ModelGirle_9() const { return ___ModelGirle_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ModelGirle_9() { return &___ModelGirle_9; }
	inline void set_ModelGirle_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ModelGirle_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModelGirle_9), (void*)value);
	}

	inline static int32_t get_offset_of_ModelMan_10() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___ModelMan_10)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_ModelMan_10() const { return ___ModelMan_10; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_ModelMan_10() { return &___ModelMan_10; }
	inline void set_ModelMan_10(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___ModelMan_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModelMan_10), (void*)value);
	}

	inline static int32_t get_offset_of_ModelRotate_11() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___ModelRotate_11)); }
	inline CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * get_ModelRotate_11() const { return ___ModelRotate_11; }
	inline CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD ** get_address_of_ModelRotate_11() { return &___ModelRotate_11; }
	inline void set_ModelRotate_11(CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * value)
	{
		___ModelRotate_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ModelRotate_11), (void*)value);
	}

	inline static int32_t get_offset_of_Gender_12() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Gender_12)); }
	inline bool get_Gender_12() const { return ___Gender_12; }
	inline bool* get_address_of_Gender_12() { return &___Gender_12; }
	inline void set_Gender_12(bool value)
	{
		___Gender_12 = value;
	}

	inline static int32_t get_offset_of_Het_13() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Het_13)); }
	inline int32_t get_Het_13() const { return ___Het_13; }
	inline int32_t* get_address_of_Het_13() { return &___Het_13; }
	inline void set_Het_13(int32_t value)
	{
		___Het_13 = value;
	}

	inline static int32_t get_offset_of_Shirt_14() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Shirt_14)); }
	inline int32_t get_Shirt_14() const { return ___Shirt_14; }
	inline int32_t* get_address_of_Shirt_14() { return &___Shirt_14; }
	inline void set_Shirt_14(int32_t value)
	{
		___Shirt_14 = value;
	}

	inline static int32_t get_offset_of_Pants_15() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Pants_15)); }
	inline int32_t get_Pants_15() const { return ___Pants_15; }
	inline int32_t* get_address_of_Pants_15() { return &___Pants_15; }
	inline void set_Pants_15(int32_t value)
	{
		___Pants_15 = value;
	}

	inline static int32_t get_offset_of_Bag_16() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Bag_16)); }
	inline int32_t get_Bag_16() const { return ___Bag_16; }
	inline int32_t* get_address_of_Bag_16() { return &___Bag_16; }
	inline void set_Bag_16(int32_t value)
	{
		___Bag_16 = value;
	}

	inline static int32_t get_offset_of_Accesories_17() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Accesories_17)); }
	inline int32_t get_Accesories_17() const { return ___Accesories_17; }
	inline int32_t* get_address_of_Accesories_17() { return &___Accesories_17; }
	inline void set_Accesories_17(int32_t value)
	{
		___Accesories_17 = value;
	}

	inline static int32_t get_offset_of_Shoe_18() { return static_cast<int32_t>(offsetof(CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4, ___Shoe_18)); }
	inline int32_t get_Shoe_18() const { return ___Shoe_18; }
	inline int32_t* get_address_of_Shoe_18() { return &___Shoe_18; }
	inline void set_Shoe_18(int32_t value)
	{
		___Shoe_18 = value;
	}
};


// ModelDetails
struct ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.GameObject[] ModelDetails::PartHat
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___PartHat_4;
	// UnityEngine.GameObject[] ModelDetails::PartShirt
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___PartShirt_5;
	// UnityEngine.GameObject[] ModelDetails::PartPants
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___PartPants_6;
	// UnityEngine.GameObject[] ModelDetails::PartBag
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___PartBag_7;
	// UnityEngine.GameObject[] ModelDetails::PartAccessories
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___PartAccessories_8;
	// UnityEngine.GameObject[] ModelDetails::PartShoe
	GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* ___PartShoe_9;

public:
	inline static int32_t get_offset_of_PartHat_4() { return static_cast<int32_t>(offsetof(ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6, ___PartHat_4)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_PartHat_4() const { return ___PartHat_4; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_PartHat_4() { return &___PartHat_4; }
	inline void set_PartHat_4(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___PartHat_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PartHat_4), (void*)value);
	}

	inline static int32_t get_offset_of_PartShirt_5() { return static_cast<int32_t>(offsetof(ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6, ___PartShirt_5)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_PartShirt_5() const { return ___PartShirt_5; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_PartShirt_5() { return &___PartShirt_5; }
	inline void set_PartShirt_5(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___PartShirt_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PartShirt_5), (void*)value);
	}

	inline static int32_t get_offset_of_PartPants_6() { return static_cast<int32_t>(offsetof(ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6, ___PartPants_6)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_PartPants_6() const { return ___PartPants_6; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_PartPants_6() { return &___PartPants_6; }
	inline void set_PartPants_6(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___PartPants_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PartPants_6), (void*)value);
	}

	inline static int32_t get_offset_of_PartBag_7() { return static_cast<int32_t>(offsetof(ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6, ___PartBag_7)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_PartBag_7() const { return ___PartBag_7; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_PartBag_7() { return &___PartBag_7; }
	inline void set_PartBag_7(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___PartBag_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PartBag_7), (void*)value);
	}

	inline static int32_t get_offset_of_PartAccessories_8() { return static_cast<int32_t>(offsetof(ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6, ___PartAccessories_8)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_PartAccessories_8() const { return ___PartAccessories_8; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_PartAccessories_8() { return &___PartAccessories_8; }
	inline void set_PartAccessories_8(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___PartAccessories_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PartAccessories_8), (void*)value);
	}

	inline static int32_t get_offset_of_PartShoe_9() { return static_cast<int32_t>(offsetof(ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6, ___PartShoe_9)); }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* get_PartShoe_9() const { return ___PartShoe_9; }
	inline GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642** get_address_of_PartShoe_9() { return &___PartShoe_9; }
	inline void set_PartShoe_9(GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* value)
	{
		___PartShoe_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PartShoe_9), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UISpriteSwap
struct UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Sprite UISpriteSwap::MainImage
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___MainImage_4;
	// UnityEngine.Sprite UISpriteSwap::SelectionImage
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___SelectionImage_5;
	// System.Boolean UISpriteSwap::Ready
	bool ___Ready_6;
	// UnityEngine.UI.Button UISpriteSwap::ButtonUI
	Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * ___ButtonUI_7;

public:
	inline static int32_t get_offset_of_MainImage_4() { return static_cast<int32_t>(offsetof(UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883, ___MainImage_4)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_MainImage_4() const { return ___MainImage_4; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_MainImage_4() { return &___MainImage_4; }
	inline void set_MainImage_4(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___MainImage_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MainImage_4), (void*)value);
	}

	inline static int32_t get_offset_of_SelectionImage_5() { return static_cast<int32_t>(offsetof(UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883, ___SelectionImage_5)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_SelectionImage_5() const { return ___SelectionImage_5; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_SelectionImage_5() { return &___SelectionImage_5; }
	inline void set_SelectionImage_5(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___SelectionImage_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___SelectionImage_5), (void*)value);
	}

	inline static int32_t get_offset_of_Ready_6() { return static_cast<int32_t>(offsetof(UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883, ___Ready_6)); }
	inline bool get_Ready_6() const { return ___Ready_6; }
	inline bool* get_address_of_Ready_6() { return &___Ready_6; }
	inline void set_Ready_6(bool value)
	{
		___Ready_6 = value;
	}

	inline static int32_t get_offset_of_ButtonUI_7() { return static_cast<int32_t>(offsetof(UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883, ___ButtonUI_7)); }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * get_ButtonUI_7() const { return ___ButtonUI_7; }
	inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D ** get_address_of_ButtonUI_7() { return &___ButtonUI_7; }
	inline void set_ButtonUI_7(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * value)
	{
		___ButtonUI_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___ButtonUI_7), (void*)value);
	}
};


// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::m_Material
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_Material_6;
	// UnityEngine.Color UnityEngine.UI.Graphic::m_Color
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_Color_7;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipLayoutUpdate
	bool ___m_SkipLayoutUpdate_8;
	// System.Boolean UnityEngine.UI.Graphic::m_SkipMaterialUpdate
	bool ___m_SkipMaterialUpdate_9;
	// System.Boolean UnityEngine.UI.Graphic::m_RaycastTarget
	bool ___m_RaycastTarget_10;
	// UnityEngine.Vector4 UnityEngine.UI.Graphic::m_RaycastPadding
	Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  ___m_RaycastPadding_11;
	// UnityEngine.RectTransform UnityEngine.UI.Graphic::m_RectTransform
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_RectTransform_12;
	// UnityEngine.CanvasRenderer UnityEngine.UI.Graphic::m_CanvasRenderer
	CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * ___m_CanvasRenderer_13;
	// UnityEngine.Canvas UnityEngine.UI.Graphic::m_Canvas
	Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * ___m_Canvas_14;
	// System.Boolean UnityEngine.UI.Graphic::m_VertsDirty
	bool ___m_VertsDirty_15;
	// System.Boolean UnityEngine.UI.Graphic::m_MaterialDirty
	bool ___m_MaterialDirty_16;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyLayoutCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyLayoutCallback_17;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyVertsCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyVertsCallback_18;
	// UnityEngine.Events.UnityAction UnityEngine.UI.Graphic::m_OnDirtyMaterialCallback
	UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * ___m_OnDirtyMaterialCallback_19;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::m_CachedMesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___m_CachedMesh_22;
	// UnityEngine.Vector2[] UnityEngine.UI.Graphic::m_CachedUvs
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___m_CachedUvs_23;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.ColorTween> UnityEngine.UI.Graphic::m_ColorTweenRunner
	TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * ___m_ColorTweenRunner_24;
	// System.Boolean UnityEngine.UI.Graphic::<useLegacyMeshGeneration>k__BackingField
	bool ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25;

public:
	inline static int32_t get_offset_of_m_Material_6() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Material_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_Material_6() const { return ___m_Material_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_Material_6() { return &___m_Material_6; }
	inline void set_m_Material_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_Material_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Material_6), (void*)value);
	}

	inline static int32_t get_offset_of_m_Color_7() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Color_7)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_Color_7() const { return ___m_Color_7; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_Color_7() { return &___m_Color_7; }
	inline void set_m_Color_7(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_Color_7 = value;
	}

	inline static int32_t get_offset_of_m_SkipLayoutUpdate_8() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipLayoutUpdate_8)); }
	inline bool get_m_SkipLayoutUpdate_8() const { return ___m_SkipLayoutUpdate_8; }
	inline bool* get_address_of_m_SkipLayoutUpdate_8() { return &___m_SkipLayoutUpdate_8; }
	inline void set_m_SkipLayoutUpdate_8(bool value)
	{
		___m_SkipLayoutUpdate_8 = value;
	}

	inline static int32_t get_offset_of_m_SkipMaterialUpdate_9() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_SkipMaterialUpdate_9)); }
	inline bool get_m_SkipMaterialUpdate_9() const { return ___m_SkipMaterialUpdate_9; }
	inline bool* get_address_of_m_SkipMaterialUpdate_9() { return &___m_SkipMaterialUpdate_9; }
	inline void set_m_SkipMaterialUpdate_9(bool value)
	{
		___m_SkipMaterialUpdate_9 = value;
	}

	inline static int32_t get_offset_of_m_RaycastTarget_10() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastTarget_10)); }
	inline bool get_m_RaycastTarget_10() const { return ___m_RaycastTarget_10; }
	inline bool* get_address_of_m_RaycastTarget_10() { return &___m_RaycastTarget_10; }
	inline void set_m_RaycastTarget_10(bool value)
	{
		___m_RaycastTarget_10 = value;
	}

	inline static int32_t get_offset_of_m_RaycastPadding_11() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RaycastPadding_11)); }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  get_m_RaycastPadding_11() const { return ___m_RaycastPadding_11; }
	inline Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7 * get_address_of_m_RaycastPadding_11() { return &___m_RaycastPadding_11; }
	inline void set_m_RaycastPadding_11(Vector4_tA56A37FC5661BCC89C3DDC24BE12BA5BCB6A02C7  value)
	{
		___m_RaycastPadding_11 = value;
	}

	inline static int32_t get_offset_of_m_RectTransform_12() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_RectTransform_12)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_RectTransform_12() const { return ___m_RectTransform_12; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_RectTransform_12() { return &___m_RectTransform_12; }
	inline void set_m_RectTransform_12(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_RectTransform_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_RectTransform_12), (void*)value);
	}

	inline static int32_t get_offset_of_m_CanvasRenderer_13() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CanvasRenderer_13)); }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * get_m_CanvasRenderer_13() const { return ___m_CanvasRenderer_13; }
	inline CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E ** get_address_of_m_CanvasRenderer_13() { return &___m_CanvasRenderer_13; }
	inline void set_m_CanvasRenderer_13(CanvasRenderer_tCF8ABE659F7C3A6ED0D99A988D0BDFB651310F0E * value)
	{
		___m_CanvasRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasRenderer_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_Canvas_14() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_Canvas_14)); }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * get_m_Canvas_14() const { return ___m_Canvas_14; }
	inline Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA ** get_address_of_m_Canvas_14() { return &___m_Canvas_14; }
	inline void set_m_Canvas_14(Canvas_t2B7E56B7BDC287962E092755372E214ACB6393EA * value)
	{
		___m_Canvas_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Canvas_14), (void*)value);
	}

	inline static int32_t get_offset_of_m_VertsDirty_15() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_VertsDirty_15)); }
	inline bool get_m_VertsDirty_15() const { return ___m_VertsDirty_15; }
	inline bool* get_address_of_m_VertsDirty_15() { return &___m_VertsDirty_15; }
	inline void set_m_VertsDirty_15(bool value)
	{
		___m_VertsDirty_15 = value;
	}

	inline static int32_t get_offset_of_m_MaterialDirty_16() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_MaterialDirty_16)); }
	inline bool get_m_MaterialDirty_16() const { return ___m_MaterialDirty_16; }
	inline bool* get_address_of_m_MaterialDirty_16() { return &___m_MaterialDirty_16; }
	inline void set_m_MaterialDirty_16(bool value)
	{
		___m_MaterialDirty_16 = value;
	}

	inline static int32_t get_offset_of_m_OnDirtyLayoutCallback_17() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyLayoutCallback_17)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyLayoutCallback_17() const { return ___m_OnDirtyLayoutCallback_17; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyLayoutCallback_17() { return &___m_OnDirtyLayoutCallback_17; }
	inline void set_m_OnDirtyLayoutCallback_17(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyLayoutCallback_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyLayoutCallback_17), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyVertsCallback_18() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyVertsCallback_18)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyVertsCallback_18() const { return ___m_OnDirtyVertsCallback_18; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyVertsCallback_18() { return &___m_OnDirtyVertsCallback_18; }
	inline void set_m_OnDirtyVertsCallback_18(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyVertsCallback_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyVertsCallback_18), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnDirtyMaterialCallback_19() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_OnDirtyMaterialCallback_19)); }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * get_m_OnDirtyMaterialCallback_19() const { return ___m_OnDirtyMaterialCallback_19; }
	inline UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 ** get_address_of_m_OnDirtyMaterialCallback_19() { return &___m_OnDirtyMaterialCallback_19; }
	inline void set_m_OnDirtyMaterialCallback_19(UnityAction_t22E545F8BE0A62EE051C6A83E209587A0DB1C099 * value)
	{
		___m_OnDirtyMaterialCallback_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnDirtyMaterialCallback_19), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedMesh_22() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedMesh_22)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_m_CachedMesh_22() const { return ___m_CachedMesh_22; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_m_CachedMesh_22() { return &___m_CachedMesh_22; }
	inline void set_m_CachedMesh_22(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___m_CachedMesh_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedMesh_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_CachedUvs_23() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_CachedUvs_23)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_m_CachedUvs_23() const { return ___m_CachedUvs_23; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_m_CachedUvs_23() { return &___m_CachedUvs_23; }
	inline void set_m_CachedUvs_23(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___m_CachedUvs_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CachedUvs_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ColorTweenRunner_24() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___m_ColorTweenRunner_24)); }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * get_m_ColorTweenRunner_24() const { return ___m_ColorTweenRunner_24; }
	inline TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 ** get_address_of_m_ColorTweenRunner_24() { return &___m_ColorTweenRunner_24; }
	inline void set_m_ColorTweenRunner_24(TweenRunner_1_tD84B9953874682FCC36990AF2C54D748293908F3 * value)
	{
		___m_ColorTweenRunner_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ColorTweenRunner_24), (void*)value);
	}

	inline static int32_t get_offset_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24, ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25)); }
	inline bool get_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() const { return ___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline bool* get_address_of_U3CuseLegacyMeshGenerationU3Ek__BackingField_25() { return &___U3CuseLegacyMeshGenerationU3Ek__BackingField_25; }
	inline void set_U3CuseLegacyMeshGenerationU3Ek__BackingField_25(bool value)
	{
		___U3CuseLegacyMeshGenerationU3Ek__BackingField_25 = value;
	}
};

struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Graphic::s_DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_DefaultUI_4;
	// UnityEngine.Texture2D UnityEngine.UI.Graphic::s_WhiteTexture
	Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * ___s_WhiteTexture_5;
	// UnityEngine.Mesh UnityEngine.UI.Graphic::s_Mesh
	Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * ___s_Mesh_20;
	// UnityEngine.UI.VertexHelper UnityEngine.UI.Graphic::s_VertexHelper
	VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * ___s_VertexHelper_21;

public:
	inline static int32_t get_offset_of_s_DefaultUI_4() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_DefaultUI_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_DefaultUI_4() const { return ___s_DefaultUI_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_DefaultUI_4() { return &___s_DefaultUI_4; }
	inline void set_s_DefaultUI_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_DefaultUI_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_DefaultUI_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_WhiteTexture_5() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_WhiteTexture_5)); }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * get_s_WhiteTexture_5() const { return ___s_WhiteTexture_5; }
	inline Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF ** get_address_of_s_WhiteTexture_5() { return &___s_WhiteTexture_5; }
	inline void set_s_WhiteTexture_5(Texture2D_t9B604D0D8E28032123641A7E7338FA872E2698BF * value)
	{
		___s_WhiteTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_WhiteTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of_s_Mesh_20() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_Mesh_20)); }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * get_s_Mesh_20() const { return ___s_Mesh_20; }
	inline Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 ** get_address_of_s_Mesh_20() { return &___s_Mesh_20; }
	inline void set_s_Mesh_20(Mesh_t2F5992DBA650D5862B43D3823ACD997132A57DA6 * value)
	{
		___s_Mesh_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Mesh_20), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertexHelper_21() { return static_cast<int32_t>(offsetof(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24_StaticFields, ___s_VertexHelper_21)); }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * get_s_VertexHelper_21() const { return ___s_VertexHelper_21; }
	inline VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 ** get_address_of_s_VertexHelper_21() { return &___s_VertexHelper_21; }
	inline void set_s_VertexHelper_21(VertexHelper_tDE8B67D3B076061C4F8DF325B0D63ED2E5367E55 * value)
	{
		___s_VertexHelper_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertexHelper_21), (void*)value);
	}
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Button
struct Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.UI.Button/ButtonClickedEvent UnityEngine.UI.Button::m_OnClick
	ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * ___m_OnClick_20;

public:
	inline static int32_t get_offset_of_m_OnClick_20() { return static_cast<int32_t>(offsetof(Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D, ___m_OnClick_20)); }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * get_m_OnClick_20() const { return ___m_OnClick_20; }
	inline ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F ** get_address_of_m_OnClick_20() { return &___m_OnClick_20; }
	inline void set_m_OnClick_20(ButtonClickedEvent_tE6D6D94ED8100451CF00D2BED1FB2253F37BB14F * value)
	{
		___m_OnClick_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnClick_20), (void*)value);
	}
};


// UnityEngine.UI.MaskableGraphic
struct MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE  : public Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24
{
public:
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculateStencil
	bool ___m_ShouldRecalculateStencil_26;
	// UnityEngine.Material UnityEngine.UI.MaskableGraphic::m_MaskMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___m_MaskMaterial_27;
	// UnityEngine.UI.RectMask2D UnityEngine.UI.MaskableGraphic::m_ParentMask
	RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * ___m_ParentMask_28;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_Maskable
	bool ___m_Maskable_29;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IsMaskingGraphic
	bool ___m_IsMaskingGraphic_30;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_IncludeForMasking
	bool ___m_IncludeForMasking_31;
	// UnityEngine.UI.MaskableGraphic/CullStateChangedEvent UnityEngine.UI.MaskableGraphic::m_OnCullStateChanged
	CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * ___m_OnCullStateChanged_32;
	// System.Boolean UnityEngine.UI.MaskableGraphic::m_ShouldRecalculate
	bool ___m_ShouldRecalculate_33;
	// System.Int32 UnityEngine.UI.MaskableGraphic::m_StencilValue
	int32_t ___m_StencilValue_34;
	// UnityEngine.Vector3[] UnityEngine.UI.MaskableGraphic::m_Corners
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___m_Corners_35;

public:
	inline static int32_t get_offset_of_m_ShouldRecalculateStencil_26() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculateStencil_26)); }
	inline bool get_m_ShouldRecalculateStencil_26() const { return ___m_ShouldRecalculateStencil_26; }
	inline bool* get_address_of_m_ShouldRecalculateStencil_26() { return &___m_ShouldRecalculateStencil_26; }
	inline void set_m_ShouldRecalculateStencil_26(bool value)
	{
		___m_ShouldRecalculateStencil_26 = value;
	}

	inline static int32_t get_offset_of_m_MaskMaterial_27() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_MaskMaterial_27)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_m_MaskMaterial_27() const { return ___m_MaskMaterial_27; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_m_MaskMaterial_27() { return &___m_MaskMaterial_27; }
	inline void set_m_MaskMaterial_27(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___m_MaskMaterial_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_MaskMaterial_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_ParentMask_28() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ParentMask_28)); }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * get_m_ParentMask_28() const { return ___m_ParentMask_28; }
	inline RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 ** get_address_of_m_ParentMask_28() { return &___m_ParentMask_28; }
	inline void set_m_ParentMask_28(RectMask2D_tD909811991B341D752E4C978C89EFB80FA7A2B15 * value)
	{
		___m_ParentMask_28 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ParentMask_28), (void*)value);
	}

	inline static int32_t get_offset_of_m_Maskable_29() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Maskable_29)); }
	inline bool get_m_Maskable_29() const { return ___m_Maskable_29; }
	inline bool* get_address_of_m_Maskable_29() { return &___m_Maskable_29; }
	inline void set_m_Maskable_29(bool value)
	{
		___m_Maskable_29 = value;
	}

	inline static int32_t get_offset_of_m_IsMaskingGraphic_30() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IsMaskingGraphic_30)); }
	inline bool get_m_IsMaskingGraphic_30() const { return ___m_IsMaskingGraphic_30; }
	inline bool* get_address_of_m_IsMaskingGraphic_30() { return &___m_IsMaskingGraphic_30; }
	inline void set_m_IsMaskingGraphic_30(bool value)
	{
		___m_IsMaskingGraphic_30 = value;
	}

	inline static int32_t get_offset_of_m_IncludeForMasking_31() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_IncludeForMasking_31)); }
	inline bool get_m_IncludeForMasking_31() const { return ___m_IncludeForMasking_31; }
	inline bool* get_address_of_m_IncludeForMasking_31() { return &___m_IncludeForMasking_31; }
	inline void set_m_IncludeForMasking_31(bool value)
	{
		___m_IncludeForMasking_31 = value;
	}

	inline static int32_t get_offset_of_m_OnCullStateChanged_32() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_OnCullStateChanged_32)); }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * get_m_OnCullStateChanged_32() const { return ___m_OnCullStateChanged_32; }
	inline CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 ** get_address_of_m_OnCullStateChanged_32() { return &___m_OnCullStateChanged_32; }
	inline void set_m_OnCullStateChanged_32(CullStateChangedEvent_t9B69755DEBEF041C3CC15C3604610BDD72856BD4 * value)
	{
		___m_OnCullStateChanged_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnCullStateChanged_32), (void*)value);
	}

	inline static int32_t get_offset_of_m_ShouldRecalculate_33() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_ShouldRecalculate_33)); }
	inline bool get_m_ShouldRecalculate_33() const { return ___m_ShouldRecalculate_33; }
	inline bool* get_address_of_m_ShouldRecalculate_33() { return &___m_ShouldRecalculate_33; }
	inline void set_m_ShouldRecalculate_33(bool value)
	{
		___m_ShouldRecalculate_33 = value;
	}

	inline static int32_t get_offset_of_m_StencilValue_34() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_StencilValue_34)); }
	inline int32_t get_m_StencilValue_34() const { return ___m_StencilValue_34; }
	inline int32_t* get_address_of_m_StencilValue_34() { return &___m_StencilValue_34; }
	inline void set_m_StencilValue_34(int32_t value)
	{
		___m_StencilValue_34 = value;
	}

	inline static int32_t get_offset_of_m_Corners_35() { return static_cast<int32_t>(offsetof(MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE, ___m_Corners_35)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_m_Corners_35() const { return ___m_Corners_35; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_m_Corners_35() { return &___m_Corners_35; }
	inline void set_m_Corners_35(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___m_Corners_35 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Corners_35), (void*)value);
	}
};


// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C  : public MaskableGraphic_t0DB59E37E3C8AD2F5A4FB7FB091630CB21370CCE
{
public:
	// UnityEngine.Sprite UnityEngine.UI.Image::m_Sprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Sprite_37;
	// UnityEngine.Sprite UnityEngine.UI.Image::m_OverrideSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_OverrideSprite_38;
	// UnityEngine.UI.Image/Type UnityEngine.UI.Image::m_Type
	int32_t ___m_Type_39;
	// System.Boolean UnityEngine.UI.Image::m_PreserveAspect
	bool ___m_PreserveAspect_40;
	// System.Boolean UnityEngine.UI.Image::m_FillCenter
	bool ___m_FillCenter_41;
	// UnityEngine.UI.Image/FillMethod UnityEngine.UI.Image::m_FillMethod
	int32_t ___m_FillMethod_42;
	// System.Single UnityEngine.UI.Image::m_FillAmount
	float ___m_FillAmount_43;
	// System.Boolean UnityEngine.UI.Image::m_FillClockwise
	bool ___m_FillClockwise_44;
	// System.Int32 UnityEngine.UI.Image::m_FillOrigin
	int32_t ___m_FillOrigin_45;
	// System.Single UnityEngine.UI.Image::m_AlphaHitTestMinimumThreshold
	float ___m_AlphaHitTestMinimumThreshold_46;
	// System.Boolean UnityEngine.UI.Image::m_Tracked
	bool ___m_Tracked_47;
	// System.Boolean UnityEngine.UI.Image::m_UseSpriteMesh
	bool ___m_UseSpriteMesh_48;
	// System.Single UnityEngine.UI.Image::m_PixelsPerUnitMultiplier
	float ___m_PixelsPerUnitMultiplier_49;
	// System.Single UnityEngine.UI.Image::m_CachedReferencePixelsPerUnit
	float ___m_CachedReferencePixelsPerUnit_50;

public:
	inline static int32_t get_offset_of_m_Sprite_37() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Sprite_37)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Sprite_37() const { return ___m_Sprite_37; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Sprite_37() { return &___m_Sprite_37; }
	inline void set_m_Sprite_37(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Sprite_37 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Sprite_37), (void*)value);
	}

	inline static int32_t get_offset_of_m_OverrideSprite_38() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_OverrideSprite_38)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_OverrideSprite_38() const { return ___m_OverrideSprite_38; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_OverrideSprite_38() { return &___m_OverrideSprite_38; }
	inline void set_m_OverrideSprite_38(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_OverrideSprite_38 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OverrideSprite_38), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type_39() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Type_39)); }
	inline int32_t get_m_Type_39() const { return ___m_Type_39; }
	inline int32_t* get_address_of_m_Type_39() { return &___m_Type_39; }
	inline void set_m_Type_39(int32_t value)
	{
		___m_Type_39 = value;
	}

	inline static int32_t get_offset_of_m_PreserveAspect_40() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PreserveAspect_40)); }
	inline bool get_m_PreserveAspect_40() const { return ___m_PreserveAspect_40; }
	inline bool* get_address_of_m_PreserveAspect_40() { return &___m_PreserveAspect_40; }
	inline void set_m_PreserveAspect_40(bool value)
	{
		___m_PreserveAspect_40 = value;
	}

	inline static int32_t get_offset_of_m_FillCenter_41() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillCenter_41)); }
	inline bool get_m_FillCenter_41() const { return ___m_FillCenter_41; }
	inline bool* get_address_of_m_FillCenter_41() { return &___m_FillCenter_41; }
	inline void set_m_FillCenter_41(bool value)
	{
		___m_FillCenter_41 = value;
	}

	inline static int32_t get_offset_of_m_FillMethod_42() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillMethod_42)); }
	inline int32_t get_m_FillMethod_42() const { return ___m_FillMethod_42; }
	inline int32_t* get_address_of_m_FillMethod_42() { return &___m_FillMethod_42; }
	inline void set_m_FillMethod_42(int32_t value)
	{
		___m_FillMethod_42 = value;
	}

	inline static int32_t get_offset_of_m_FillAmount_43() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillAmount_43)); }
	inline float get_m_FillAmount_43() const { return ___m_FillAmount_43; }
	inline float* get_address_of_m_FillAmount_43() { return &___m_FillAmount_43; }
	inline void set_m_FillAmount_43(float value)
	{
		___m_FillAmount_43 = value;
	}

	inline static int32_t get_offset_of_m_FillClockwise_44() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillClockwise_44)); }
	inline bool get_m_FillClockwise_44() const { return ___m_FillClockwise_44; }
	inline bool* get_address_of_m_FillClockwise_44() { return &___m_FillClockwise_44; }
	inline void set_m_FillClockwise_44(bool value)
	{
		___m_FillClockwise_44 = value;
	}

	inline static int32_t get_offset_of_m_FillOrigin_45() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_FillOrigin_45)); }
	inline int32_t get_m_FillOrigin_45() const { return ___m_FillOrigin_45; }
	inline int32_t* get_address_of_m_FillOrigin_45() { return &___m_FillOrigin_45; }
	inline void set_m_FillOrigin_45(int32_t value)
	{
		___m_FillOrigin_45 = value;
	}

	inline static int32_t get_offset_of_m_AlphaHitTestMinimumThreshold_46() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_AlphaHitTestMinimumThreshold_46)); }
	inline float get_m_AlphaHitTestMinimumThreshold_46() const { return ___m_AlphaHitTestMinimumThreshold_46; }
	inline float* get_address_of_m_AlphaHitTestMinimumThreshold_46() { return &___m_AlphaHitTestMinimumThreshold_46; }
	inline void set_m_AlphaHitTestMinimumThreshold_46(float value)
	{
		___m_AlphaHitTestMinimumThreshold_46 = value;
	}

	inline static int32_t get_offset_of_m_Tracked_47() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_Tracked_47)); }
	inline bool get_m_Tracked_47() const { return ___m_Tracked_47; }
	inline bool* get_address_of_m_Tracked_47() { return &___m_Tracked_47; }
	inline void set_m_Tracked_47(bool value)
	{
		___m_Tracked_47 = value;
	}

	inline static int32_t get_offset_of_m_UseSpriteMesh_48() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_UseSpriteMesh_48)); }
	inline bool get_m_UseSpriteMesh_48() const { return ___m_UseSpriteMesh_48; }
	inline bool* get_address_of_m_UseSpriteMesh_48() { return &___m_UseSpriteMesh_48; }
	inline void set_m_UseSpriteMesh_48(bool value)
	{
		___m_UseSpriteMesh_48 = value;
	}

	inline static int32_t get_offset_of_m_PixelsPerUnitMultiplier_49() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_PixelsPerUnitMultiplier_49)); }
	inline float get_m_PixelsPerUnitMultiplier_49() const { return ___m_PixelsPerUnitMultiplier_49; }
	inline float* get_address_of_m_PixelsPerUnitMultiplier_49() { return &___m_PixelsPerUnitMultiplier_49; }
	inline void set_m_PixelsPerUnitMultiplier_49(float value)
	{
		___m_PixelsPerUnitMultiplier_49 = value;
	}

	inline static int32_t get_offset_of_m_CachedReferencePixelsPerUnit_50() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C, ___m_CachedReferencePixelsPerUnit_50)); }
	inline float get_m_CachedReferencePixelsPerUnit_50() const { return ___m_CachedReferencePixelsPerUnit_50; }
	inline float* get_address_of_m_CachedReferencePixelsPerUnit_50() { return &___m_CachedReferencePixelsPerUnit_50; }
	inline void set_m_CachedReferencePixelsPerUnit_50(float value)
	{
		___m_CachedReferencePixelsPerUnit_50 = value;
	}
};

struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields
{
public:
	// UnityEngine.Material UnityEngine.UI.Image::s_ETC1DefaultUI
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___s_ETC1DefaultUI_36;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_VertScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_VertScratch_51;
	// UnityEngine.Vector2[] UnityEngine.UI.Image::s_UVScratch
	Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* ___s_UVScratch_52;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Xy
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Xy_53;
	// UnityEngine.Vector3[] UnityEngine.UI.Image::s_Uv
	Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* ___s_Uv_54;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> UnityEngine.UI.Image::m_TrackedTexturelessImages
	List_1_t815A476B0A21E183042059E705F9E505478CD8AE * ___m_TrackedTexturelessImages_55;
	// System.Boolean UnityEngine.UI.Image::s_Initialized
	bool ___s_Initialized_56;

public:
	inline static int32_t get_offset_of_s_ETC1DefaultUI_36() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_ETC1DefaultUI_36)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_s_ETC1DefaultUI_36() const { return ___s_ETC1DefaultUI_36; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_s_ETC1DefaultUI_36() { return &___s_ETC1DefaultUI_36; }
	inline void set_s_ETC1DefaultUI_36(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___s_ETC1DefaultUI_36 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_ETC1DefaultUI_36), (void*)value);
	}

	inline static int32_t get_offset_of_s_VertScratch_51() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_VertScratch_51)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_VertScratch_51() const { return ___s_VertScratch_51; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_VertScratch_51() { return &___s_VertScratch_51; }
	inline void set_s_VertScratch_51(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_VertScratch_51 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_VertScratch_51), (void*)value);
	}

	inline static int32_t get_offset_of_s_UVScratch_52() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_UVScratch_52)); }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* get_s_UVScratch_52() const { return ___s_UVScratch_52; }
	inline Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA** get_address_of_s_UVScratch_52() { return &___s_UVScratch_52; }
	inline void set_s_UVScratch_52(Vector2U5BU5D_tE0F58A2D6D8592B5EC37D9CDEF09103A02E5D7FA* value)
	{
		___s_UVScratch_52 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_UVScratch_52), (void*)value);
	}

	inline static int32_t get_offset_of_s_Xy_53() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Xy_53)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Xy_53() const { return ___s_Xy_53; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Xy_53() { return &___s_Xy_53; }
	inline void set_s_Xy_53(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Xy_53 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Xy_53), (void*)value);
	}

	inline static int32_t get_offset_of_s_Uv_54() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Uv_54)); }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* get_s_Uv_54() const { return ___s_Uv_54; }
	inline Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4** get_address_of_s_Uv_54() { return &___s_Uv_54; }
	inline void set_s_Uv_54(Vector3U5BU5D_t5FB88EAA33E46838BDC2ABDAEA3E8727491CB9E4* value)
	{
		___s_Uv_54 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Uv_54), (void*)value);
	}

	inline static int32_t get_offset_of_m_TrackedTexturelessImages_55() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___m_TrackedTexturelessImages_55)); }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE * get_m_TrackedTexturelessImages_55() const { return ___m_TrackedTexturelessImages_55; }
	inline List_1_t815A476B0A21E183042059E705F9E505478CD8AE ** get_address_of_m_TrackedTexturelessImages_55() { return &___m_TrackedTexturelessImages_55; }
	inline void set_m_TrackedTexturelessImages_55(List_1_t815A476B0A21E183042059E705F9E505478CD8AE * value)
	{
		___m_TrackedTexturelessImages_55 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TrackedTexturelessImages_55), (void*)value);
	}

	inline static int32_t get_offset_of_s_Initialized_56() { return static_cast<int32_t>(offsetof(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C_StaticFields, ___s_Initialized_56)); }
	inline bool get_s_Initialized_56() const { return ___s_Initialized_56; }
	inline bool* get_address_of_s_Initialized_56() { return &___s_Initialized_56; }
	inline void set_s_Initialized_56(bool value)
	{
		___s_Initialized_56 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * m_Items[1];

public:
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<CharacterItem>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  JsonUtility_FromJson_TisCharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_mE6DE82CCF8F3E3A7EAB924FA577A0DB5A541FA59_gshared (String_t* ___json0, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<CharacterLastItem>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  JsonUtility_FromJson_TisCharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_mB4EEF589EB7B5E5D751D8DBEC8DE65025380131E_gshared (String_t* ___json0, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AndroidJavaObject_GetStatic_TisRuntimeObject_mEC743ECF275CB896DE039A9FC1E5672B30C8B3D0_gshared (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * __this, String_t* ___fieldName0, const RuntimeMethod* method);

// System.Int32 UnityEngine.Screen::get_width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C (const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3 (int32_t ___button0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E (const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.Component::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1 (int32_t ___button0, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Subtraction(UnityEngine.Vector3,UnityEngine.Vector3)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::get_down()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629 (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Vector3::op_Multiply(UnityEngine.Vector3,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::Euler(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___euler0, const RuntimeMethod* method);
// UnityEngine.Quaternion UnityEngine.Quaternion::op_Multiply(UnityEngine.Quaternion,UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___lhs0, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___rhs1, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_rotation(UnityEngine.Quaternion)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B (Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<CharacterRotate>()
inline CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * GameObject_GetComponent_TisCharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD_m4D544A881C6D200B0CFF1470B48069BF0CFEB20A (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void ExtensionCusTomsCharacter::StartScene(CusTomsCharacter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_StartScene_mC0988804556E30B98C5D352E867E7F601F3E892B (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * ___customs0, const RuntimeMethod* method);
// System.Void CusTomsCharacter::UpdateModel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_UpdateModel_mAB1ED5E45E097B9B4DFED4580ACE63DC01598095 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method);
// System.Void ExtensionCusTomsCharacter::UpdateItemToNative(CusTomsCharacter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_UpdateItemToNative_mADD4924A4A10948BC4CAF637DC30ACFBCA95C756 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * ___customs0, const RuntimeMethod* method);
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, bool ___value0, const RuntimeMethod* method);
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Vector3::.ctor(System.Single,System.Single,System.Single)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___value0, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<ModelDetails>()
inline ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * GameObject_GetComponent_TisModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6_m9A3AC35D76078DA89CD45B502D62582166B20518 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.Camera UnityEngine.Camera::get_main()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C (const RuntimeMethod* method);
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::LookAt(UnityEngine.Vector3)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___worldPosition0, const RuntimeMethod* method);
// System.Void UnityEngine.Transform::Rotate(System.Single,System.Single,System.Single,UnityEngine.Space)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357 (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * __this, float ___xAngle0, float ___yAngle1, float ___zAngle2, int32_t ___relativeTo3, const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UISpriteSwap>()
inline UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883 * GameObject_GetComponent_TisUISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883_m46590BE8DCFBDA4B920ADF4F183ED1E84BABA64B (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883 * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// System.Void CusTomsCharacter::CustomUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_CustomUpdate_mABF82C935643D4243E52751A6C310EED32CE279D (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method);
// System.Void ExtensionCusTomsCharacter::LastSelectItem(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_LastSelectItem_mD147F8F18FEC6C1E58A810A4E1DD76FA9CFCBCE1 (int32_t ___numberSelect0, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<CharacterItem>(System.String)
inline CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  JsonUtility_FromJson_TisCharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_mE6DE82CCF8F3E3A7EAB924FA577A0DB5A541FA59 (String_t* ___json0, const RuntimeMethod* method)
{
	return ((  CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  (*) (String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisCharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_mE6DE82CCF8F3E3A7EAB924FA577A0DB5A541FA59_gshared)(___json0, method);
}
// System.Void UnityEngine.Debug::Log(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8 (RuntimeObject * ___message0, const RuntimeMethod* method);
// System.Void CusTomsCharacter::UpdateWithJson(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_UpdateWithJson_mD5D1E3F428BB38704DF94C78C80FF6EFD07CA228 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, String_t* ___json0, const RuntimeMethod* method);
// System.Void CusTomsCharacter::SetMenuBar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_SetMenuBar_m712FE3131208BC62D1527F5EF06A2D922231D741 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method);
// !!0 UnityEngine.JsonUtility::FromJson<CharacterLastItem>(System.String)
inline CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  JsonUtility_FromJson_TisCharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_mB4EEF589EB7B5E5D751D8DBEC8DE65025380131E (String_t* ___json0, const RuntimeMethod* method)
{
	return ((  CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  (*) (String_t*, const RuntimeMethod*))JsonUtility_FromJson_TisCharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_mB4EEF589EB7B5E5D751D8DBEC8DE65025380131E_gshared)(___json0, method);
}
// System.Int32 ExtensionCusTomsCharacter::ToInt(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ExtensionCusTomsCharacter_ToInt_m2554F281D955C23A229781EBF24BFEB2410F3FDA (bool ___value0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void ExtensionCusTomsCharacter::ResetPositionCharacter(UnityEngine.Transform,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_ResetPositionCharacter_mF8AA537F5FF98FC57700354E2F3DABFDA635DB9B (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, float ___x1, const RuntimeMethod* method);
// System.String UnityEngine.JsonUtility::ToJson(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* JsonUtility_ToJson_mF4F097C9AEC7699970E3E7E99EF8FF2F44DA1B5C (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void ExtensionCusTomsCharacter/NativeAPI::SendToNative(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_SendToNative_mE1A3D6410681C7761C6F163B61D1B731995B79F1 (String_t* ___message0, const RuntimeMethod* method);
// CharacterLastItem ExtensionCusTomsCharacter::ConvertToCharacterLastItem(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  ExtensionCusTomsCharacter_ConvertToCharacterLastItem_mC446324403D52ECFDB3430A23A2022A4E0640FA9 (int32_t ___Num0, const RuntimeMethod* method);
// System.Void ExtensionCusTomsCharacter/NativeAPI::LastSelectItem(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_LastSelectItem_mE97F9CE03B9769E54869571EC1AD9A8CAEAC6B68 (String_t* ___characterLastItem0, const RuntimeMethod* method);
// CharacterLastItem ExtensionCusTomsCharacter::MakeCharacterLastItem(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C (String_t* ___itemName0, int32_t ___numberId1, const RuntimeMethod* method);
// System.Void UnityEngine.Quaternion::.ctor(System.Single,System.Single,System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421 (Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * __this, float ___x0, float ___y1, float ___z2, float ___w3, const RuntimeMethod* method);
// System.Void ExtensionCusTomsCharacter/NativeAPI::StartScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_StartScene_m213C8305BFD872BC5ECB55E49ACF01DA1F88C15E (const RuntimeMethod* method);
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Button>()
inline Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8 (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * __this, const RuntimeMethod* method)
{
	return ((  Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * (*) (GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 *, const RuntimeMethod*))GameObject_GetComponent_TisRuntimeObject_mCE43118393A796C759AC5D43257AB2330881767D_gshared)(__this, method);
}
// UnityEngine.UI.Image UnityEngine.UI.Selectable::get_image()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * Selectable_get_image_mAB45C107C7C858ECBEFFFF540B8C69746BB6C6FE (Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Image::set_sprite(UnityEngine.Sprite)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4 (Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * __this, Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.AndroidJavaClass::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaClass__ctor_mEFF9F51871F231955D97DABDE9AB4A6B4EDA5541 (AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * __this, String_t* ___className0, const RuntimeMethod* method);
// !!0 UnityEngine.AndroidJavaObject::GetStatic<UnityEngine.AndroidJavaObject>(System.String)
inline AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_mC84C97A7EC20ED712D21107C9FA32E0785021153 (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * __this, String_t* ___fieldName0, const RuntimeMethod* method)
{
	return ((  AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * (*) (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E *, String_t*, const RuntimeMethod*))AndroidJavaObject_GetStatic_TisRuntimeObject_mEC743ECF275CB896DE039A9FC1E5672B30C8B3D0_gshared)(__this, ___fieldName0, method);
}
// System.Void UnityEngine.AndroidJavaObject::Call(System.String,System.Object[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AndroidJavaObject_Call_mBB226DA52CE5A2FCD9A2D42BC7FB4272E094B76D (AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * __this, String_t* ___methodName0, ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___args1, const RuntimeMethod* method);
// System.Void ExtensionCusTomsCharacter/NativeAPI::CallAndroidMethod(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_CallAndroidMethod_m309542DA3F713FB85889EFEFF13A15580E579A83 (String_t* ___methodName0, RuntimeObject * ___str1, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: CharacterItem
IL2CPP_EXTERN_C void CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshal_pinvoke(const CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1& unmarshaled, CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_pinvoke& marshaled)
{
	marshaled.___gender_0 = static_cast<int32_t>(unmarshaled.get_gender_0());
	marshaled.___hat_1 = unmarshaled.get_hat_1();
	marshaled.___shirt_2 = unmarshaled.get_shirt_2();
	marshaled.___pants_3 = unmarshaled.get_pants_3();
	marshaled.___bag_4 = unmarshaled.get_bag_4();
	marshaled.___accesories_5 = unmarshaled.get_accesories_5();
	marshaled.___shoe_6 = unmarshaled.get_shoe_6();
}
IL2CPP_EXTERN_C void CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshal_pinvoke_back(const CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_pinvoke& marshaled, CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1& unmarshaled)
{
	bool unmarshaled_gender_temp_0 = false;
	unmarshaled_gender_temp_0 = static_cast<bool>(marshaled.___gender_0);
	unmarshaled.set_gender_0(unmarshaled_gender_temp_0);
	int32_t unmarshaled_hat_temp_1 = 0;
	unmarshaled_hat_temp_1 = marshaled.___hat_1;
	unmarshaled.set_hat_1(unmarshaled_hat_temp_1);
	int32_t unmarshaled_shirt_temp_2 = 0;
	unmarshaled_shirt_temp_2 = marshaled.___shirt_2;
	unmarshaled.set_shirt_2(unmarshaled_shirt_temp_2);
	int32_t unmarshaled_pants_temp_3 = 0;
	unmarshaled_pants_temp_3 = marshaled.___pants_3;
	unmarshaled.set_pants_3(unmarshaled_pants_temp_3);
	int32_t unmarshaled_bag_temp_4 = 0;
	unmarshaled_bag_temp_4 = marshaled.___bag_4;
	unmarshaled.set_bag_4(unmarshaled_bag_temp_4);
	int32_t unmarshaled_accesories_temp_5 = 0;
	unmarshaled_accesories_temp_5 = marshaled.___accesories_5;
	unmarshaled.set_accesories_5(unmarshaled_accesories_temp_5);
	int32_t unmarshaled_shoe_temp_6 = 0;
	unmarshaled_shoe_temp_6 = marshaled.___shoe_6;
	unmarshaled.set_shoe_6(unmarshaled_shoe_temp_6);
}
// Conversion method for clean up from marshalling of: CharacterItem
IL2CPP_EXTERN_C void CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshal_pinvoke_cleanup(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: CharacterItem
IL2CPP_EXTERN_C void CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshal_com(const CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1& unmarshaled, CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_com& marshaled)
{
	marshaled.___gender_0 = static_cast<int32_t>(unmarshaled.get_gender_0());
	marshaled.___hat_1 = unmarshaled.get_hat_1();
	marshaled.___shirt_2 = unmarshaled.get_shirt_2();
	marshaled.___pants_3 = unmarshaled.get_pants_3();
	marshaled.___bag_4 = unmarshaled.get_bag_4();
	marshaled.___accesories_5 = unmarshaled.get_accesories_5();
	marshaled.___shoe_6 = unmarshaled.get_shoe_6();
}
IL2CPP_EXTERN_C void CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshal_com_back(const CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_com& marshaled, CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1& unmarshaled)
{
	bool unmarshaled_gender_temp_0 = false;
	unmarshaled_gender_temp_0 = static_cast<bool>(marshaled.___gender_0);
	unmarshaled.set_gender_0(unmarshaled_gender_temp_0);
	int32_t unmarshaled_hat_temp_1 = 0;
	unmarshaled_hat_temp_1 = marshaled.___hat_1;
	unmarshaled.set_hat_1(unmarshaled_hat_temp_1);
	int32_t unmarshaled_shirt_temp_2 = 0;
	unmarshaled_shirt_temp_2 = marshaled.___shirt_2;
	unmarshaled.set_shirt_2(unmarshaled_shirt_temp_2);
	int32_t unmarshaled_pants_temp_3 = 0;
	unmarshaled_pants_temp_3 = marshaled.___pants_3;
	unmarshaled.set_pants_3(unmarshaled_pants_temp_3);
	int32_t unmarshaled_bag_temp_4 = 0;
	unmarshaled_bag_temp_4 = marshaled.___bag_4;
	unmarshaled.set_bag_4(unmarshaled_bag_temp_4);
	int32_t unmarshaled_accesories_temp_5 = 0;
	unmarshaled_accesories_temp_5 = marshaled.___accesories_5;
	unmarshaled.set_accesories_5(unmarshaled_accesories_temp_5);
	int32_t unmarshaled_shoe_temp_6 = 0;
	unmarshaled_shoe_temp_6 = marshaled.___shoe_6;
	unmarshaled.set_shoe_6(unmarshaled_shoe_temp_6);
}
// Conversion method for clean up from marshalling of: CharacterItem
IL2CPP_EXTERN_C void CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshal_com_cleanup(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: CharacterLastItem
IL2CPP_EXTERN_C void CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshal_pinvoke(const CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E& unmarshaled, CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_pinvoke& marshaled)
{
	marshaled.___itemName_0 = il2cpp_codegen_marshal_string(unmarshaled.get_itemName_0());
	marshaled.___isActive_1 = static_cast<int32_t>(unmarshaled.get_isActive_1());
}
IL2CPP_EXTERN_C void CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshal_pinvoke_back(const CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_pinvoke& marshaled, CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E& unmarshaled)
{
	unmarshaled.set_itemName_0(il2cpp_codegen_marshal_string_result(marshaled.___itemName_0));
	bool unmarshaled_isActive_temp_1 = false;
	unmarshaled_isActive_temp_1 = static_cast<bool>(marshaled.___isActive_1);
	unmarshaled.set_isActive_1(unmarshaled_isActive_temp_1);
}
// Conversion method for clean up from marshalling of: CharacterLastItem
IL2CPP_EXTERN_C void CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshal_pinvoke_cleanup(CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_pinvoke& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___itemName_0);
	marshaled.___itemName_0 = NULL;
}
// Conversion methods for marshalling of: CharacterLastItem
IL2CPP_EXTERN_C void CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshal_com(const CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E& unmarshaled, CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_com& marshaled)
{
	marshaled.___itemName_0 = il2cpp_codegen_marshal_bstring(unmarshaled.get_itemName_0());
	marshaled.___isActive_1 = static_cast<int32_t>(unmarshaled.get_isActive_1());
}
IL2CPP_EXTERN_C void CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshal_com_back(const CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_com& marshaled, CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E& unmarshaled)
{
	unmarshaled.set_itemName_0(il2cpp_codegen_marshal_bstring_result(marshaled.___itemName_0));
	bool unmarshaled_isActive_temp_1 = false;
	unmarshaled_isActive_temp_1 = static_cast<bool>(marshaled.___isActive_1);
	unmarshaled.set_isActive_1(unmarshaled_isActive_temp_1);
}
// Conversion method for clean up from marshalling of: CharacterLastItem
IL2CPP_EXTERN_C void CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshal_com_cleanup(CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_marshaled_com& marshaled)
{
	il2cpp_codegen_marshal_free_bstring(marshaled.___itemName_0);
	marshaled.___itemName_0 = NULL;
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CharacterRotate::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterRotate_Start_m5C3A32BDAB1E800791F0A79627E15062D9776C5D (CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * __this, const RuntimeMethod* method)
{
	{
		// SceneWidth = Screen.width;
		int32_t L_0;
		L_0 = Screen_get_width_m52188F76E8AAF57BE373018CB14083BB74C43C1C(/*hidden argument*/NULL);
		__this->set_SceneWidth_4(((float)((float)L_0)));
		// }
		return;
	}
}
// System.Void CharacterRotate::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterRotate_Update_mA2295A96DDDBC839DE76C863701DF2D0AAE481C5 (CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	float V_2 = 0.0f;
	{
		// if (Input.GetMouseButtonDown(0))
		bool L_0;
		L_0 = Input_GetMouseButtonDown_m466D81FDCC87C9CB07557B39DCB435EB691F1EF3(0, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_002b;
		}
	}
	{
		// PressPoint = Input.mousePosition;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		L_2 = Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E(/*hidden argument*/NULL);
		__this->set_PressPoint_5(L_2);
		// StartRotation = transform.rotation;
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3;
		L_3 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_4;
		L_4 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_3, /*hidden argument*/NULL);
		__this->set_StartRotation_6(L_4);
		goto IL_0085;
	}

IL_002b:
	{
		// else if (Input.GetMouseButton(0))
		bool L_5;
		L_5 = Input_GetMouseButton_m27BF2DDBF38A38787B83A13D3E6F0F88F7C834C1(0, /*hidden argument*/NULL);
		V_1 = L_5;
		bool L_6 = V_1;
		if (!L_6)
		{
			goto IL_0085;
		}
	}
	{
		// float CurrentDistanceBetweenMousePositions = (Input.mousePosition - PressPoint).x;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_7;
		L_7 = Input_get_mousePosition_m79528BC2F30C57054641F709C855130AE586AC0E(/*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = __this->get_PressPoint_5();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		L_9 = Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline(L_7, L_8, /*hidden argument*/NULL);
		float L_10 = L_9.get_x_2();
		V_2 = L_10;
		// transform.rotation = StartRotation * Quaternion.Euler(Vector3.down * (CurrentDistanceBetweenMousePositions / SceneWidth) * 360);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(__this, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_12 = __this->get_StartRotation_6();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13;
		L_13 = Vector3_get_down_mFA85B870E184121D30F66395BB183ECAB9DD8629(/*hidden argument*/NULL);
		float L_14 = V_2;
		float L_15 = __this->get_SceneWidth_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_16;
		L_16 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_13, ((float)((float)L_14/(float)L_15)), /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_17;
		L_17 = Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline(L_16, (360.0f), /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_18;
		L_18 = Quaternion_Euler_m887ABE4F4DD563351E9874D63922C2F53969BBAB(L_17, /*hidden argument*/NULL);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_19;
		L_19 = Quaternion_op_Multiply_m5C7A60AC0CDCA2C5E2F23E45FBD1B15CA152D7B0(L_12, L_18, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_11, L_19, /*hidden argument*/NULL);
	}

IL_0085:
	{
		// }
		return;
	}
}
// System.Void CharacterRotate::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CharacterRotate__ctor_m96B237055E719DC8DEA6E7BBAADCFE2C240BD6C4 (CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void CusTomsCharacter::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_Start_m1126B9F99F522F16DEADE8B558CE58834CCDADCF (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisCharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD_m4D544A881C6D200B0CFF1470B48069BF0CFEB20A_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ModelRotate = gameObject.GetComponent<CharacterRotate>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		CharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD * L_1;
		L_1 = GameObject_GetComponent_TisCharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD_m4D544A881C6D200B0CFF1470B48069BF0CFEB20A(L_0, /*hidden argument*/GameObject_GetComponent_TisCharacterRotate_t8F20735ACEEBCC2DBD792449B57CB2A45AF7B7DD_m4D544A881C6D200B0CFF1470B48069BF0CFEB20A_RuntimeMethod_var);
		__this->set_ModelRotate_11(L_1);
		// this.StartScene();
		ExtensionCusTomsCharacter_StartScene_mC0988804556E30B98C5D352E867E7F601F3E892B(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CusTomsCharacter::CustomUpdate()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_CustomUpdate_mABF82C935643D4243E52751A6C310EED32CE279D (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method)
{
	{
		// UpdateModel();
		CusTomsCharacter_UpdateModel_mAB1ED5E45E097B9B4DFED4580ACE63DC01598095(__this, /*hidden argument*/NULL);
		// this.UpdateItemToNative();
		ExtensionCusTomsCharacter_UpdateItemToNative_mADD4924A4A10948BC4CAF637DC30ACFBCA95C756(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CusTomsCharacter::UpdateModel()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_UpdateModel_mAB1ED5E45E097B9B4DFED4580ACE63DC01598095 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6_m9A3AC35D76078DA89CD45B502D62582166B20518_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * V_0 = NULL;
	bool V_1 = false;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * V_2 = NULL;
	Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * V_3 = NULL;
	int32_t V_4 = 0;
	bool V_5 = false;
	bool V_6 = false;
	int32_t V_7 = 0;
	bool V_8 = false;
	bool V_9 = false;
	int32_t V_10 = 0;
	bool V_11 = false;
	bool V_12 = false;
	int32_t V_13 = 0;
	bool V_14 = false;
	bool V_15 = false;
	int32_t V_16 = 0;
	bool V_17 = false;
	bool V_18 = false;
	int32_t V_19 = 0;
	bool V_20 = false;
	bool V_21 = false;
	{
		// if (Gender)
		bool L_0 = __this->get_Gender_12();
		V_1 = L_0;
		bool L_1 = V_1;
		if (!L_1)
		{
			goto IL_00b4;
		}
	}
	{
		// ModelMan.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_ModelMan_10();
		NullCheck(L_2);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)1, /*hidden argument*/NULL);
		// ModelGirle.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_ModelGirle_9();
		NullCheck(L_3);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)0, /*hidden argument*/NULL);
		// ModelMan.transform.position = new Vector3(0, 0, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_ModelMan_10();
		NullCheck(L_4);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_5;
		L_5 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_4, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6;
		memset((&L_6), 0, sizeof(L_6));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_6), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_5, L_6, /*hidden argument*/NULL);
		// Model = ModelMan.GetComponent<ModelDetails>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_ModelMan_10();
		NullCheck(L_7);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_8;
		L_8 = GameObject_GetComponent_TisModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6_m9A3AC35D76078DA89CD45B502D62582166B20518(L_7, /*hidden argument*/GameObject_GetComponent_TisModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6_m9A3AC35D76078DA89CD45B502D62582166B20518_RuntimeMethod_var);
		V_0 = L_8;
		// Camera CameraMain = Camera.main;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_9;
		L_9 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		V_2 = L_9;
		// CameraMain.transform.LookAt(ModelMan.transform.position);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_10 = V_2;
		NullCheck(L_10);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_11;
		L_11 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_10, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_12 = __this->get_ModelMan_10();
		NullCheck(L_12);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_13;
		L_13 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_12, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_14;
		L_14 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA(L_11, L_14, /*hidden argument*/NULL);
		// CameraMain.transform.Rotate(-10.0f, CameraMain.transform.rotation.y, CameraMain.transform.rotation.z, Space.Self);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_15 = V_2;
		NullCheck(L_15);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_16;
		L_16 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_15, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_17 = V_2;
		NullCheck(L_17);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_18;
		L_18 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_17, /*hidden argument*/NULL);
		NullCheck(L_18);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_19;
		L_19 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_18, /*hidden argument*/NULL);
		float L_20 = L_19.get_y_1();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_21 = V_2;
		NullCheck(L_21);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_22;
		L_22 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_23;
		L_23 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_22, /*hidden argument*/NULL);
		float L_24 = L_23.get_z_2();
		NullCheck(L_16);
		Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357(L_16, (-10.0f), L_20, L_24, 1, /*hidden argument*/NULL);
		goto IL_0155;
	}

IL_00b4:
	{
		// ModelMan.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_25 = __this->get_ModelMan_10();
		NullCheck(L_25);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_25, (bool)0, /*hidden argument*/NULL);
		// ModelGirle.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_26 = __this->get_ModelGirle_9();
		NullCheck(L_26);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_26, (bool)1, /*hidden argument*/NULL);
		// ModelGirle.transform.position = new Vector3(0, 0, 0);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_27 = __this->get_ModelGirle_9();
		NullCheck(L_27);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_28;
		L_28 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_27, /*hidden argument*/NULL);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_29;
		memset((&L_29), 0, sizeof(L_29));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_29), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_28);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_28, L_29, /*hidden argument*/NULL);
		// Model = ModelGirle.GetComponent<ModelDetails>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_30 = __this->get_ModelGirle_9();
		NullCheck(L_30);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_31;
		L_31 = GameObject_GetComponent_TisModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6_m9A3AC35D76078DA89CD45B502D62582166B20518(L_30, /*hidden argument*/GameObject_GetComponent_TisModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6_m9A3AC35D76078DA89CD45B502D62582166B20518_RuntimeMethod_var);
		V_0 = L_31;
		// Camera CameraMain = Camera.main;
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_32;
		L_32 = Camera_get_main_mC337C621B91591CEF89504C97EF64D717C12871C(/*hidden argument*/NULL);
		V_3 = L_32;
		// CameraMain.transform.LookAt(ModelGirle.transform.position);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_33 = V_3;
		NullCheck(L_33);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_34;
		L_34 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_33, /*hidden argument*/NULL);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_35 = __this->get_ModelGirle_9();
		NullCheck(L_35);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_36;
		L_36 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_37;
		L_37 = Transform_get_position_m40A8A9895568D56FFC687B57F30E8D53CB5EA341(L_36, /*hidden argument*/NULL);
		NullCheck(L_34);
		Transform_LookAt_m996FADE2327B0A4412FF4A5179B8BABD9EB849BA(L_34, L_37, /*hidden argument*/NULL);
		// CameraMain.transform.Rotate(-10.0f, CameraMain.transform.rotation.y, CameraMain.transform.rotation.z, Space.Self);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_38 = V_3;
		NullCheck(L_38);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_39;
		L_39 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_38, /*hidden argument*/NULL);
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_40 = V_3;
		NullCheck(L_40);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_41;
		L_41 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_40, /*hidden argument*/NULL);
		NullCheck(L_41);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_42;
		L_42 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_41, /*hidden argument*/NULL);
		float L_43 = L_42.get_y_1();
		Camera_tC44E094BAB53AFC8A014C6F9CFCE11F4FC38006C * L_44 = V_3;
		NullCheck(L_44);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_45;
		L_45 = Component_get_transform_mE8496EBC45BEB1BADB5F314960F1DF1C952FA11F(L_44, /*hidden argument*/NULL);
		NullCheck(L_45);
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_46;
		L_46 = Transform_get_rotation_m4AA3858C00DF4C9614B80352558C4C37D08D2200(L_45, /*hidden argument*/NULL);
		float L_47 = L_46.get_z_2();
		NullCheck(L_39);
		Transform_Rotate_mE77655C011C18F49CAD740CED7940EF1C7000357(L_39, (-10.0f), L_43, L_47, 1, /*hidden argument*/NULL);
	}

IL_0155:
	{
		// for (int i = 0; i < Model.PartHat.Length; i++)
		V_4 = 0;
		goto IL_0198;
	}

IL_015a:
	{
		// if (Het == i)
		int32_t L_48 = __this->get_Het_13();
		int32_t L_49 = V_4;
		V_5 = (bool)((((int32_t)L_48) == ((int32_t)L_49))? 1 : 0);
		bool L_50 = V_5;
		if (!L_50)
		{
			goto IL_017f;
		}
	}
	{
		// Model.PartHat[i].SetActive(true);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_51 = V_0;
		NullCheck(L_51);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_52 = L_51->get_PartHat_4();
		int32_t L_53 = V_4;
		NullCheck(L_52);
		int32_t L_54 = L_53;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_55 = (L_52)->GetAt(static_cast<il2cpp_array_size_t>(L_54));
		NullCheck(L_55);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_55, (bool)1, /*hidden argument*/NULL);
		goto IL_0191;
	}

IL_017f:
	{
		// Model.PartHat[i].SetActive(false);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_56 = V_0;
		NullCheck(L_56);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_57 = L_56->get_PartHat_4();
		int32_t L_58 = V_4;
		NullCheck(L_57);
		int32_t L_59 = L_58;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_60 = (L_57)->GetAt(static_cast<il2cpp_array_size_t>(L_59));
		NullCheck(L_60);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_60, (bool)0, /*hidden argument*/NULL);
	}

IL_0191:
	{
		// for (int i = 0; i < Model.PartHat.Length; i++)
		int32_t L_61 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_61, (int32_t)1));
	}

IL_0198:
	{
		// for (int i = 0; i < Model.PartHat.Length; i++)
		int32_t L_62 = V_4;
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_63 = V_0;
		NullCheck(L_63);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_64 = L_63->get_PartHat_4();
		NullCheck(L_64);
		V_6 = (bool)((((int32_t)L_62) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_64)->max_length)))))? 1 : 0);
		bool L_65 = V_6;
		if (L_65)
		{
			goto IL_015a;
		}
	}
	{
		// for (int i = 0; i < Model.PartShirt.Length; i++)
		V_7 = 0;
		goto IL_01ed;
	}

IL_01af:
	{
		// if (Shirt == i)
		int32_t L_66 = __this->get_Shirt_14();
		int32_t L_67 = V_7;
		V_8 = (bool)((((int32_t)L_66) == ((int32_t)L_67))? 1 : 0);
		bool L_68 = V_8;
		if (!L_68)
		{
			goto IL_01d4;
		}
	}
	{
		// Model.PartShirt[i].SetActive(true);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_69 = V_0;
		NullCheck(L_69);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_70 = L_69->get_PartShirt_5();
		int32_t L_71 = V_7;
		NullCheck(L_70);
		int32_t L_72 = L_71;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_73 = (L_70)->GetAt(static_cast<il2cpp_array_size_t>(L_72));
		NullCheck(L_73);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_73, (bool)1, /*hidden argument*/NULL);
		goto IL_01e6;
	}

IL_01d4:
	{
		// Model.PartShirt[i].SetActive(false);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_74 = V_0;
		NullCheck(L_74);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_75 = L_74->get_PartShirt_5();
		int32_t L_76 = V_7;
		NullCheck(L_75);
		int32_t L_77 = L_76;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_78 = (L_75)->GetAt(static_cast<il2cpp_array_size_t>(L_77));
		NullCheck(L_78);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_78, (bool)0, /*hidden argument*/NULL);
	}

IL_01e6:
	{
		// for (int i = 0; i < Model.PartShirt.Length; i++)
		int32_t L_79 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_79, (int32_t)1));
	}

IL_01ed:
	{
		// for (int i = 0; i < Model.PartShirt.Length; i++)
		int32_t L_80 = V_7;
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_81 = V_0;
		NullCheck(L_81);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_82 = L_81->get_PartShirt_5();
		NullCheck(L_82);
		V_9 = (bool)((((int32_t)L_80) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_82)->max_length)))))? 1 : 0);
		bool L_83 = V_9;
		if (L_83)
		{
			goto IL_01af;
		}
	}
	{
		// for (int i = 0; i < Model.PartPants.Length; i++)
		V_10 = 0;
		goto IL_0242;
	}

IL_0204:
	{
		// if (Pants == i)
		int32_t L_84 = __this->get_Pants_15();
		int32_t L_85 = V_10;
		V_11 = (bool)((((int32_t)L_84) == ((int32_t)L_85))? 1 : 0);
		bool L_86 = V_11;
		if (!L_86)
		{
			goto IL_0229;
		}
	}
	{
		// Model.PartPants[i].SetActive(true);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_87 = V_0;
		NullCheck(L_87);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_88 = L_87->get_PartPants_6();
		int32_t L_89 = V_10;
		NullCheck(L_88);
		int32_t L_90 = L_89;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_91 = (L_88)->GetAt(static_cast<il2cpp_array_size_t>(L_90));
		NullCheck(L_91);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_91, (bool)1, /*hidden argument*/NULL);
		goto IL_023b;
	}

IL_0229:
	{
		// Model.PartPants[i].SetActive(false);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_92 = V_0;
		NullCheck(L_92);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_93 = L_92->get_PartPants_6();
		int32_t L_94 = V_10;
		NullCheck(L_93);
		int32_t L_95 = L_94;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_96 = (L_93)->GetAt(static_cast<il2cpp_array_size_t>(L_95));
		NullCheck(L_96);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_96, (bool)0, /*hidden argument*/NULL);
	}

IL_023b:
	{
		// for (int i = 0; i < Model.PartPants.Length; i++)
		int32_t L_97 = V_10;
		V_10 = ((int32_t)il2cpp_codegen_add((int32_t)L_97, (int32_t)1));
	}

IL_0242:
	{
		// for (int i = 0; i < Model.PartPants.Length; i++)
		int32_t L_98 = V_10;
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_99 = V_0;
		NullCheck(L_99);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_100 = L_99->get_PartPants_6();
		NullCheck(L_100);
		V_12 = (bool)((((int32_t)L_98) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_100)->max_length)))))? 1 : 0);
		bool L_101 = V_12;
		if (L_101)
		{
			goto IL_0204;
		}
	}
	{
		// for (int i = 0; i < Model.PartBag.Length; i++)
		V_13 = 0;
		goto IL_0297;
	}

IL_0259:
	{
		// if (Bag == i)
		int32_t L_102 = __this->get_Bag_16();
		int32_t L_103 = V_13;
		V_14 = (bool)((((int32_t)L_102) == ((int32_t)L_103))? 1 : 0);
		bool L_104 = V_14;
		if (!L_104)
		{
			goto IL_027e;
		}
	}
	{
		// Model.PartBag[i].SetActive(true);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_105 = V_0;
		NullCheck(L_105);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_106 = L_105->get_PartBag_7();
		int32_t L_107 = V_13;
		NullCheck(L_106);
		int32_t L_108 = L_107;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_109 = (L_106)->GetAt(static_cast<il2cpp_array_size_t>(L_108));
		NullCheck(L_109);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_109, (bool)1, /*hidden argument*/NULL);
		goto IL_0290;
	}

IL_027e:
	{
		// Model.PartBag[i].SetActive(false);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_110 = V_0;
		NullCheck(L_110);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_111 = L_110->get_PartBag_7();
		int32_t L_112 = V_13;
		NullCheck(L_111);
		int32_t L_113 = L_112;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_114 = (L_111)->GetAt(static_cast<il2cpp_array_size_t>(L_113));
		NullCheck(L_114);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_114, (bool)0, /*hidden argument*/NULL);
	}

IL_0290:
	{
		// for (int i = 0; i < Model.PartBag.Length; i++)
		int32_t L_115 = V_13;
		V_13 = ((int32_t)il2cpp_codegen_add((int32_t)L_115, (int32_t)1));
	}

IL_0297:
	{
		// for (int i = 0; i < Model.PartBag.Length; i++)
		int32_t L_116 = V_13;
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_117 = V_0;
		NullCheck(L_117);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_118 = L_117->get_PartBag_7();
		NullCheck(L_118);
		V_15 = (bool)((((int32_t)L_116) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_118)->max_length)))))? 1 : 0);
		bool L_119 = V_15;
		if (L_119)
		{
			goto IL_0259;
		}
	}
	{
		// for (int i = 0; i < Model.PartAccessories.Length; i++)
		V_16 = 0;
		goto IL_02ec;
	}

IL_02ae:
	{
		// if (Accesories == i)
		int32_t L_120 = __this->get_Accesories_17();
		int32_t L_121 = V_16;
		V_17 = (bool)((((int32_t)L_120) == ((int32_t)L_121))? 1 : 0);
		bool L_122 = V_17;
		if (!L_122)
		{
			goto IL_02d3;
		}
	}
	{
		// Model.PartAccessories[i].SetActive(true);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_123 = V_0;
		NullCheck(L_123);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_124 = L_123->get_PartAccessories_8();
		int32_t L_125 = V_16;
		NullCheck(L_124);
		int32_t L_126 = L_125;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_127 = (L_124)->GetAt(static_cast<il2cpp_array_size_t>(L_126));
		NullCheck(L_127);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_127, (bool)1, /*hidden argument*/NULL);
		goto IL_02e5;
	}

IL_02d3:
	{
		// Model.PartAccessories[i].SetActive(false);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_128 = V_0;
		NullCheck(L_128);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_129 = L_128->get_PartAccessories_8();
		int32_t L_130 = V_16;
		NullCheck(L_129);
		int32_t L_131 = L_130;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_132 = (L_129)->GetAt(static_cast<il2cpp_array_size_t>(L_131));
		NullCheck(L_132);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_132, (bool)0, /*hidden argument*/NULL);
	}

IL_02e5:
	{
		// for (int i = 0; i < Model.PartAccessories.Length; i++)
		int32_t L_133 = V_16;
		V_16 = ((int32_t)il2cpp_codegen_add((int32_t)L_133, (int32_t)1));
	}

IL_02ec:
	{
		// for (int i = 0; i < Model.PartAccessories.Length; i++)
		int32_t L_134 = V_16;
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_135 = V_0;
		NullCheck(L_135);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_136 = L_135->get_PartAccessories_8();
		NullCheck(L_136);
		V_18 = (bool)((((int32_t)L_134) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_136)->max_length)))))? 1 : 0);
		bool L_137 = V_18;
		if (L_137)
		{
			goto IL_02ae;
		}
	}
	{
		// for (int i = 0; i < Model.PartShoe.Length; i++)
		V_19 = 0;
		goto IL_0341;
	}

IL_0303:
	{
		// if (Shoe == i)
		int32_t L_138 = __this->get_Shoe_18();
		int32_t L_139 = V_19;
		V_20 = (bool)((((int32_t)L_138) == ((int32_t)L_139))? 1 : 0);
		bool L_140 = V_20;
		if (!L_140)
		{
			goto IL_0328;
		}
	}
	{
		// Model.PartShoe[i].SetActive(true);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_141 = V_0;
		NullCheck(L_141);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_142 = L_141->get_PartShoe_9();
		int32_t L_143 = V_19;
		NullCheck(L_142);
		int32_t L_144 = L_143;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_145 = (L_142)->GetAt(static_cast<il2cpp_array_size_t>(L_144));
		NullCheck(L_145);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_145, (bool)1, /*hidden argument*/NULL);
		goto IL_033a;
	}

IL_0328:
	{
		// Model.PartShoe[i].SetActive(false);
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_146 = V_0;
		NullCheck(L_146);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_147 = L_146->get_PartShoe_9();
		int32_t L_148 = V_19;
		NullCheck(L_147);
		int32_t L_149 = L_148;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_150 = (L_147)->GetAt(static_cast<il2cpp_array_size_t>(L_149));
		NullCheck(L_150);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_150, (bool)0, /*hidden argument*/NULL);
	}

IL_033a:
	{
		// for (int i = 0; i < Model.PartShoe.Length; i++)
		int32_t L_151 = V_19;
		V_19 = ((int32_t)il2cpp_codegen_add((int32_t)L_151, (int32_t)1));
	}

IL_0341:
	{
		// for (int i = 0; i < Model.PartShoe.Length; i++)
		int32_t L_152 = V_19;
		ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * L_153 = V_0;
		NullCheck(L_153);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_154 = L_153->get_PartShoe_9();
		NullCheck(L_154);
		V_21 = (bool)((((int32_t)L_152) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_154)->max_length)))))? 1 : 0);
		bool L_155 = V_21;
		if (L_155)
		{
			goto IL_0303;
		}
	}
	{
		// }
		return;
	}
}
// System.Void CusTomsCharacter::ChangeBar(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_ChangeBar_mB9EB2CA8C00323C8D14F41D95B7EEF55BF033C77 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, int32_t ___Num0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisUISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883_m46590BE8DCFBDA4B920ADF4F183ED1E84BABA64B_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	bool V_1 = false;
	bool V_2 = false;
	{
		// for (int i = 0; i < Bar.Length; i++)
		V_0 = 0;
		goto IL_005f;
	}

IL_0005:
	{
		// if (i+1 == Num)
		int32_t L_0 = V_0;
		int32_t L_1 = ___Num0;
		V_1 = (bool)((((int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_0, (int32_t)1))) == ((int32_t)L_1))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0036;
		}
	}
	{
		// Bar[i].SetActive(true);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_3 = __this->get_Bar_5();
		int32_t L_4 = V_0;
		NullCheck(L_3);
		int32_t L_5 = L_4;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = (L_3)->GetAt(static_cast<il2cpp_array_size_t>(L_5));
		NullCheck(L_6);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)1, /*hidden argument*/NULL);
		// MainBar[i].GetComponent<UISpriteSwap>().Ready = true;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_7 = __this->get_MainBar_4();
		int32_t L_8 = V_0;
		NullCheck(L_7);
		int32_t L_9 = L_8;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_10 = (L_7)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		NullCheck(L_10);
		UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883 * L_11;
		L_11 = GameObject_GetComponent_TisUISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883_m46590BE8DCFBDA4B920ADF4F183ED1E84BABA64B(L_10, /*hidden argument*/GameObject_GetComponent_TisUISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883_m46590BE8DCFBDA4B920ADF4F183ED1E84BABA64B_RuntimeMethod_var);
		NullCheck(L_11);
		L_11->set_Ready_6((bool)1);
		goto IL_005a;
	}

IL_0036:
	{
		// Bar[i].SetActive(false);
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_12 = __this->get_Bar_5();
		int32_t L_13 = V_0;
		NullCheck(L_12);
		int32_t L_14 = L_13;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_15 = (L_12)->GetAt(static_cast<il2cpp_array_size_t>(L_14));
		NullCheck(L_15);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_15, (bool)0, /*hidden argument*/NULL);
		// MainBar[i].GetComponent<UISpriteSwap>().Ready = false;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_16 = __this->get_MainBar_4();
		int32_t L_17 = V_0;
		NullCheck(L_16);
		int32_t L_18 = L_17;
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_19 = (L_16)->GetAt(static_cast<il2cpp_array_size_t>(L_18));
		NullCheck(L_19);
		UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883 * L_20;
		L_20 = GameObject_GetComponent_TisUISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883_m46590BE8DCFBDA4B920ADF4F183ED1E84BABA64B(L_19, /*hidden argument*/GameObject_GetComponent_TisUISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883_m46590BE8DCFBDA4B920ADF4F183ED1E84BABA64B_RuntimeMethod_var);
		NullCheck(L_20);
		L_20->set_Ready_6((bool)0);
	}

IL_005a:
	{
		// for (int i = 0; i < Bar.Length; i++)
		int32_t L_21 = V_0;
		V_0 = ((int32_t)il2cpp_codegen_add((int32_t)L_21, (int32_t)1));
	}

IL_005f:
	{
		// for (int i = 0; i < Bar.Length; i++)
		int32_t L_22 = V_0;
		GameObjectU5BU5D_tA88FC1A1FC9D4D73D0B3984D4B0ECE88F4C47642* L_23 = __this->get_Bar_5();
		NullCheck(L_23);
		V_2 = (bool)((((int32_t)L_22) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_23)->max_length)))))? 1 : 0);
		bool L_24 = V_2;
		if (L_24)
		{
			goto IL_0005;
		}
	}
	{
		// }
		return;
	}
}
// System.Void CusTomsCharacter::ChangPart(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_ChangPart_m16EA37DB4411419A675C34159FC05CADE93591DA (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, int32_t ___Num0, const RuntimeMethod* method)
{
	bool V_0 = false;
	bool V_1 = false;
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	bool V_6 = false;
	bool V_7 = false;
	bool V_8 = false;
	{
		// if (Num / 100 == 0)
		int32_t L_0 = ___Num0;
		V_0 = (bool)((((int32_t)((int32_t)((int32_t)L_0/(int32_t)((int32_t)100)))) == ((int32_t)0))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0059;
		}
	}
	{
		// if (Num % 100 == 1)
		int32_t L_2 = ___Num0;
		V_1 = (bool)((((int32_t)((int32_t)((int32_t)L_2%(int32_t)((int32_t)100)))) == ((int32_t)1))? 1 : 0);
		bool L_3 = V_1;
		if (!L_3)
		{
			goto IL_0023;
		}
	}
	{
		// Gender = true;
		__this->set_Gender_12((bool)1);
		goto IL_002c;
	}

IL_0023:
	{
		// Gender = false;
		__this->set_Gender_12((bool)0);
	}

IL_002c:
	{
		// MainBarPlane.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_MainBarPlane_6();
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		// BarPlane.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_BarPlane_7();
		NullCheck(L_5);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_5, (bool)1, /*hidden argument*/NULL);
		// GenderPlane.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_6 = __this->get_GenderPlane_8();
		NullCheck(L_6);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_6, (bool)0, /*hidden argument*/NULL);
		goto IL_00f8;
	}

IL_0059:
	{
		// else if(Num / 100 == 1)
		int32_t L_7 = ___Num0;
		V_2 = (bool)((((int32_t)((int32_t)((int32_t)L_7/(int32_t)((int32_t)100)))) == ((int32_t)1))? 1 : 0);
		bool L_8 = V_2;
		if (!L_8)
		{
			goto IL_0075;
		}
	}
	{
		// Het = Num % 100;
		int32_t L_9 = ___Num0;
		__this->set_Het_13(((int32_t)((int32_t)L_9%(int32_t)((int32_t)100))));
		goto IL_00f8;
	}

IL_0075:
	{
		// else if (Num / 100 == 2)
		int32_t L_10 = ___Num0;
		V_3 = (bool)((((int32_t)((int32_t)((int32_t)L_10/(int32_t)((int32_t)100)))) == ((int32_t)2))? 1 : 0);
		bool L_11 = V_3;
		if (!L_11)
		{
			goto IL_008e;
		}
	}
	{
		// Shirt = Num % 100;
		int32_t L_12 = ___Num0;
		__this->set_Shirt_14(((int32_t)((int32_t)L_12%(int32_t)((int32_t)100))));
		goto IL_00f8;
	}

IL_008e:
	{
		// else if (Num / 100 == 3)
		int32_t L_13 = ___Num0;
		V_4 = (bool)((((int32_t)((int32_t)((int32_t)L_13/(int32_t)((int32_t)100)))) == ((int32_t)3))? 1 : 0);
		bool L_14 = V_4;
		if (!L_14)
		{
			goto IL_00a9;
		}
	}
	{
		// Pants = Num % 100;
		int32_t L_15 = ___Num0;
		__this->set_Pants_15(((int32_t)((int32_t)L_15%(int32_t)((int32_t)100))));
		goto IL_00f8;
	}

IL_00a9:
	{
		// else if (Num / 100 == 4)
		int32_t L_16 = ___Num0;
		V_5 = (bool)((((int32_t)((int32_t)((int32_t)L_16/(int32_t)((int32_t)100)))) == ((int32_t)4))? 1 : 0);
		bool L_17 = V_5;
		if (!L_17)
		{
			goto IL_00c4;
		}
	}
	{
		// Bag = Num % 100;
		int32_t L_18 = ___Num0;
		__this->set_Bag_16(((int32_t)((int32_t)L_18%(int32_t)((int32_t)100))));
		goto IL_00f8;
	}

IL_00c4:
	{
		// else if (Num / 100 == 5)
		int32_t L_19 = ___Num0;
		V_6 = (bool)((((int32_t)((int32_t)((int32_t)L_19/(int32_t)((int32_t)100)))) == ((int32_t)5))? 1 : 0);
		bool L_20 = V_6;
		if (!L_20)
		{
			goto IL_00df;
		}
	}
	{
		// Accesories = Num % 100;
		int32_t L_21 = ___Num0;
		__this->set_Accesories_17(((int32_t)((int32_t)L_21%(int32_t)((int32_t)100))));
		goto IL_00f8;
	}

IL_00df:
	{
		// else if (Num / 100 == 6)
		int32_t L_22 = ___Num0;
		V_7 = (bool)((((int32_t)((int32_t)((int32_t)L_22/(int32_t)((int32_t)100)))) == ((int32_t)6))? 1 : 0);
		bool L_23 = V_7;
		if (!L_23)
		{
			goto IL_00f8;
		}
	}
	{
		// Shoe = Num % 100;
		int32_t L_24 = ___Num0;
		__this->set_Shoe_18(((int32_t)((int32_t)L_24%(int32_t)((int32_t)100))));
	}

IL_00f8:
	{
		// CustomUpdate();
		CusTomsCharacter_CustomUpdate_mABF82C935643D4243E52751A6C310EED32CE279D(__this, /*hidden argument*/NULL);
		// if (Num >= 100)
		int32_t L_25 = ___Num0;
		V_8 = (bool)((((int32_t)((((int32_t)L_25) < ((int32_t)((int32_t)100)))? 1 : 0)) == ((int32_t)0))? 1 : 0);
		bool L_26 = V_8;
		if (!L_26)
		{
			goto IL_0116;
		}
	}
	{
		// Num.LastSelectItem();
		int32_t L_27 = ___Num0;
		ExtensionCusTomsCharacter_LastSelectItem_mD147F8F18FEC6C1E58A810A4E1DD76FA9CFCBCE1(L_27, /*hidden argument*/NULL);
	}

IL_0116:
	{
		// }
		return;
	}
}
// System.Void CusTomsCharacter::UpdateWithJson(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_UpdateWithJson_mD5D1E3F428BB38704DF94C78C80FF6EFD07CA228 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonUtility_FromJson_TisCharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_mE6DE82CCF8F3E3A7EAB924FA577A0DB5A541FA59_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		// var jsonObject = JsonUtility.FromJson<CharacterItem>(json);
		String_t* L_0 = ___json0;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_1;
		L_1 = JsonUtility_FromJson_TisCharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_mE6DE82CCF8F3E3A7EAB924FA577A0DB5A541FA59(L_0, /*hidden argument*/JsonUtility_FromJson_TisCharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_mE6DE82CCF8F3E3A7EAB924FA577A0DB5A541FA59_RuntimeMethod_var);
		V_0 = L_1;
		// Gender = jsonObject.gender;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_2 = V_0;
		bool L_3 = L_2.get_gender_0();
		__this->set_Gender_12(L_3);
		// Het = jsonObject.hat;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_4 = V_0;
		int32_t L_5 = L_4.get_hat_1();
		__this->set_Het_13(L_5);
		// Shirt = jsonObject.shirt;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_6 = V_0;
		int32_t L_7 = L_6.get_shirt_2();
		__this->set_Shirt_14(L_7);
		// Pants = jsonObject.pants;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_8 = V_0;
		int32_t L_9 = L_8.get_pants_3();
		__this->set_Pants_15(L_9);
		// Bag = jsonObject.bag;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_10 = V_0;
		int32_t L_11 = L_10.get_bag_4();
		__this->set_Bag_16(L_11);
		// Accesories = jsonObject.accesories;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_12 = V_0;
		int32_t L_13 = L_12.get_accesories_5();
		__this->set_Accesories_17(L_13);
		// Shoe = jsonObject.shoe;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_14 = V_0;
		int32_t L_15 = L_14.get_shoe_6();
		__this->set_Shoe_18(L_15);
		// Debug.Log(jsonObject.ToString());
		RuntimeObject * L_16 = Box(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_il2cpp_TypeInfo_var, (&V_0));
		NullCheck(L_16);
		String_t* L_17;
		L_17 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_16);
		V_0 = *(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1 *)UnBox(L_16);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_Log_mC26E5AD0D8D156C7FFD173AA15827F69225E9DB8(L_17, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CusTomsCharacter::UpdateFromNative(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_UpdateFromNative_mEF7D32CFB4E2155A68E195DB0A8299767B256384 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, String_t* ___json0, const RuntimeMethod* method)
{
	{
		// UpdateWithJson(json);
		String_t* L_0 = ___json0;
		CusTomsCharacter_UpdateWithJson_mD5D1E3F428BB38704DF94C78C80FF6EFD07CA228(__this, L_0, /*hidden argument*/NULL);
		// CustomUpdate();
		CusTomsCharacter_CustomUpdate_mABF82C935643D4243E52751A6C310EED32CE279D(__this, /*hidden argument*/NULL);
		// SetMenuBar();
		CusTomsCharacter_SetMenuBar_m712FE3131208BC62D1527F5EF06A2D922231D741(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CusTomsCharacter::SetMenuBar()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_SetMenuBar_m712FE3131208BC62D1527F5EF06A2D922231D741 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method)
{
	{
		// MainBarPlane.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_MainBarPlane_6();
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)1, /*hidden argument*/NULL);
		// BarPlane.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_BarPlane_7();
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)1, /*hidden argument*/NULL);
		// GenderPlane.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_GenderPlane_8();
		NullCheck(L_2);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CusTomsCharacter::UpdateIgnoreNative(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_UpdateIgnoreNative_mB5DA83D13368EB724DBE601D03F224873CE96562 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, String_t* ___json0, const RuntimeMethod* method)
{
	{
		// UpdateWithJson(json);
		String_t* L_0 = ___json0;
		CusTomsCharacter_UpdateWithJson_mD5D1E3F428BB38704DF94C78C80FF6EFD07CA228(__this, L_0, /*hidden argument*/NULL);
		// UpdateModel();
		CusTomsCharacter_UpdateModel_mAB1ED5E45E097B9B4DFED4580ACE63DC01598095(__this, /*hidden argument*/NULL);
		// SetMenuBar();
		CusTomsCharacter_SetMenuBar_m712FE3131208BC62D1527F5EF06A2D922231D741(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CusTomsCharacter::UpdateOneItemFromNative(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_UpdateOneItemFromNative_mB973398E0DDCF521D10D4AB02ACD5A1FF11A2EAC (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, String_t* ___json0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&JsonUtility_FromJson_TisCharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_mB4EEF589EB7B5E5D751D8DBEC8DE65025380131E_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral227DA7A8EA45379E2CD7C2CE4658EBC0AC5EC9BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2A557BCDD1470D7822A2296783B75E92CBDCB8A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50863FA6986F7E76E712DFF2F2A2AD0E3E7AE14D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9B4EB089C2114281619D0CCE8FC65CE8F7A6C788);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDB21527FC9B6F09BC7BB132750502BD3A956B226);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFAA454C5551F0BFB4AAE794589CC8CBD44FA5A4C);
		s_Il2CppMethodInitialized = true;
	}
	CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	String_t* V_2 = NULL;
	String_t* V_3 = NULL;
	{
		// var characterLastItem = JsonUtility.FromJson<CharacterLastItem>(json);
		String_t* L_0 = ___json0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_1;
		L_1 = JsonUtility_FromJson_TisCharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_mB4EEF589EB7B5E5D751D8DBEC8DE65025380131E(L_0, /*hidden argument*/JsonUtility_FromJson_TisCharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_mB4EEF589EB7B5E5D751D8DBEC8DE65025380131E_RuntimeMethod_var);
		V_0 = L_1;
		// var isActive = characterLastItem.isActive.ToInt();
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_2 = V_0;
		bool L_3 = L_2.get_isActive_1();
		int32_t L_4;
		L_4 = ExtensionCusTomsCharacter_ToInt_m2554F281D955C23A229781EBF24BFEB2410F3FDA(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// switch (characterLastItem.itemName)
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_5 = V_0;
		String_t* L_6 = L_5.get_itemName_0();
		V_3 = L_6;
		String_t* L_7 = V_3;
		V_2 = L_7;
		String_t* L_8 = V_2;
		if (!L_8)
		{
			goto IL_00a9;
		}
	}
	{
		String_t* L_9 = V_2;
		bool L_10;
		L_10 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_9, _stringLiteralFAA454C5551F0BFB4AAE794589CC8CBD44FA5A4C, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0073;
		}
	}
	{
		String_t* L_11 = V_2;
		bool L_12;
		L_12 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_11, _stringLiteralDB21527FC9B6F09BC7BB132750502BD3A956B226, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_007c;
		}
	}
	{
		String_t* L_13 = V_2;
		bool L_14;
		L_14 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_13, _stringLiteral50863FA6986F7E76E712DFF2F2A2AD0E3E7AE14D, /*hidden argument*/NULL);
		if (L_14)
		{
			goto IL_0085;
		}
	}
	{
		String_t* L_15 = V_2;
		bool L_16;
		L_16 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_15, _stringLiteral9B4EB089C2114281619D0CCE8FC65CE8F7A6C788, /*hidden argument*/NULL);
		if (L_16)
		{
			goto IL_008e;
		}
	}
	{
		String_t* L_17 = V_2;
		bool L_18;
		L_18 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_17, _stringLiteral227DA7A8EA45379E2CD7C2CE4658EBC0AC5EC9BE, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_0097;
		}
	}
	{
		String_t* L_19 = V_2;
		bool L_20;
		L_20 = String_op_Equality_m2B91EE68355F142F67095973D32EB5828B7B73CB(L_19, _stringLiteral2A557BCDD1470D7822A2296783B75E92CBDCB8A3, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_00a0;
		}
	}
	{
		goto IL_00a9;
	}

IL_0073:
	{
		// Het = isActive;
		int32_t L_21 = V_1;
		__this->set_Het_13(L_21);
		// break;
		goto IL_00ab;
	}

IL_007c:
	{
		// Accesories = isActive;
		int32_t L_22 = V_1;
		__this->set_Accesories_17(L_22);
		// break;
		goto IL_00ab;
	}

IL_0085:
	{
		// Bag = isActive;
		int32_t L_23 = V_1;
		__this->set_Bag_16(L_23);
		// break;
		goto IL_00ab;
	}

IL_008e:
	{
		// Pants = isActive;
		int32_t L_24 = V_1;
		__this->set_Pants_15(L_24);
		// break;
		goto IL_00ab;
	}

IL_0097:
	{
		// Shirt = isActive;
		int32_t L_25 = V_1;
		__this->set_Shirt_14(L_25);
		// break;
		goto IL_00ab;
	}

IL_00a0:
	{
		// Shoe = isActive;
		int32_t L_26 = V_1;
		__this->set_Shoe_18(L_26);
		// break;
		goto IL_00ab;
	}

IL_00a9:
	{
		// return;
		goto IL_00b9;
	}

IL_00ab:
	{
		// UpdateModel();
		CusTomsCharacter_UpdateModel_mAB1ED5E45E097B9B4DFED4580ACE63DC01598095(__this, /*hidden argument*/NULL);
		// SetMenuBar();
		CusTomsCharacter_SetMenuBar_m712FE3131208BC62D1527F5EF06A2D922231D741(__this, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		// }
		return;
	}
}
// System.Void CusTomsCharacter::ResetCharacter()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter_ResetCharacter_m6AE0EC86DC08D952D38B6B37AABC1888911A73E9 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method)
{
	{
		// Het = 0;
		__this->set_Het_13(0);
		// Shirt = 0;
		__this->set_Shirt_14(0);
		// Pants = 0;
		__this->set_Pants_15(0);
		// Bag = 0;
		__this->set_Bag_16(0);
		// Accesories = 0;
		__this->set_Accesories_17(0);
		// Shoe = 0;
		__this->set_Shoe_18(0);
		// CustomUpdate();
		CusTomsCharacter_CustomUpdate_mABF82C935643D4243E52751A6C310EED32CE279D(__this, /*hidden argument*/NULL);
		// MainBarPlane.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0 = __this->get_MainBarPlane_6();
		NullCheck(L_0);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_0, (bool)0, /*hidden argument*/NULL);
		// BarPlane.SetActive(false);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_1 = __this->get_BarPlane_7();
		NullCheck(L_1);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_1, (bool)0, /*hidden argument*/NULL);
		// GenderPlane.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_2 = __this->get_GenderPlane_8();
		NullCheck(L_2);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_2, (bool)1, /*hidden argument*/NULL);
		// ModelGirle.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_3 = __this->get_ModelGirle_9();
		NullCheck(L_3);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_3, (bool)1, /*hidden argument*/NULL);
		// ModelMan.SetActive(true);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_4 = __this->get_ModelMan_10();
		NullCheck(L_4);
		GameObject_SetActive_mCF1EEF2A314F3AE85DA581FF52EB06ACEF2FFF86(L_4, (bool)1, /*hidden argument*/NULL);
		// ModelMan.transform.ResetPositionCharacter(-1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_5 = __this->get_ModelMan_10();
		NullCheck(L_5);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_6;
		L_6 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_5, /*hidden argument*/NULL);
		ExtensionCusTomsCharacter_ResetPositionCharacter_mF8AA537F5FF98FC57700354E2F3DABFDA635DB9B(L_6, (-1.0f), /*hidden argument*/NULL);
		// ModelGirle.transform.ResetPositionCharacter(1);
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_7 = __this->get_ModelGirle_9();
		NullCheck(L_7);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_8;
		L_8 = GameObject_get_transform_m16A80BB92B6C8C5AB696E447014D45EDF1E4DE34(L_7, /*hidden argument*/NULL);
		ExtensionCusTomsCharacter_ResetPositionCharacter_mF8AA537F5FF98FC57700354E2F3DABFDA635DB9B(L_8, (1.0f), /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void CusTomsCharacter::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CusTomsCharacter__ctor_mFF497519CBCA0F5AB1DECD378624F568ED02C47F (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ExtensionCusTomsCharacter::UpdateItemToNative(CusTomsCharacter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_UpdateItemToNative_mADD4924A4A10948BC4CAF637DC30ACFBCA95C756 (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * ___customs0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  V_0;
	memset((&V_0), 0, sizeof(V_0));
	CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// var itemList = new CharacterItem()
		// {
		//     gender = customs.Gender,
		//     hat = customs.Het,
		//     shirt = customs.Shirt,
		//     pants = customs.Pants,
		//     bag = customs.Bag,
		//     accesories = customs.Accesories,
		//     shoe = customs.Shoe
		// 
		// };
		il2cpp_codegen_initobj((&V_1), sizeof(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1 ));
		CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * L_0 = ___customs0;
		NullCheck(L_0);
		bool L_1 = L_0->get_Gender_12();
		(&V_1)->set_gender_0(L_1);
		CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * L_2 = ___customs0;
		NullCheck(L_2);
		int32_t L_3 = L_2->get_Het_13();
		(&V_1)->set_hat_1(L_3);
		CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * L_4 = ___customs0;
		NullCheck(L_4);
		int32_t L_5 = L_4->get_Shirt_14();
		(&V_1)->set_shirt_2(L_5);
		CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * L_6 = ___customs0;
		NullCheck(L_6);
		int32_t L_7 = L_6->get_Pants_15();
		(&V_1)->set_pants_3(L_7);
		CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * L_8 = ___customs0;
		NullCheck(L_8);
		int32_t L_9 = L_8->get_Bag_16();
		(&V_1)->set_bag_4(L_9);
		CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * L_10 = ___customs0;
		NullCheck(L_10);
		int32_t L_11 = L_10->get_Accesories_17();
		(&V_1)->set_accesories_5(L_11);
		CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * L_12 = ___customs0;
		NullCheck(L_12);
		int32_t L_13 = L_12->get_Shoe_18();
		(&V_1)->set_shoe_6(L_13);
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_14 = V_1;
		V_0 = L_14;
		// NativeAPI.SendToNative(JsonUtility.ToJson(itemList));
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_15 = V_0;
		CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1  L_16 = L_15;
		RuntimeObject * L_17 = Box(CharacterItem_t5FFD229E5103AF0F9E5B218C753DDD7D15E438E1_il2cpp_TypeInfo_var, &L_16);
		String_t* L_18;
		L_18 = JsonUtility_ToJson_mF4F097C9AEC7699970E3E7E99EF8FF2F44DA1B5C(L_17, /*hidden argument*/NULL);
		NativeAPI_SendToNative_mE1A3D6410681C7761C6F163B61D1B731995B79F1(L_18, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ExtensionCusTomsCharacter::LastSelectItem(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_LastSelectItem_mD147F8F18FEC6C1E58A810A4E1DD76FA9CFCBCE1 (int32_t ___numberSelect0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_il2cpp_TypeInfo_var);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	{
		// var json = JsonUtility.ToJson(ConvertToCharacterLastItem(numberSelect));
		int32_t L_0 = ___numberSelect0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_1;
		L_1 = ExtensionCusTomsCharacter_ConvertToCharacterLastItem_mC446324403D52ECFDB3430A23A2022A4E0640FA9(L_0, /*hidden argument*/NULL);
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_2 = L_1;
		RuntimeObject * L_3 = Box(CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E_il2cpp_TypeInfo_var, &L_2);
		String_t* L_4;
		L_4 = JsonUtility_ToJson_mF4F097C9AEC7699970E3E7E99EF8FF2F44DA1B5C(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		// NativeAPI.LastSelectItem(json);
		String_t* L_5 = V_0;
		NativeAPI_LastSelectItem_mE97F9CE03B9769E54869571EC1AD9A8CAEAC6B68(L_5, /*hidden argument*/NULL);
		// }
		return;
	}
}
// CharacterLastItem ExtensionCusTomsCharacter::ConvertToCharacterLastItem(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  ExtensionCusTomsCharacter_ConvertToCharacterLastItem_mC446324403D52ECFDB3430A23A2022A4E0640FA9 (int32_t ___Num0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral227DA7A8EA45379E2CD7C2CE4658EBC0AC5EC9BE);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral2A557BCDD1470D7822A2296783B75E92CBDCB8A3);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral50863FA6986F7E76E712DFF2F2A2AD0E3E7AE14D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral9B4EB089C2114281619D0CCE8FC65CE8F7A6C788);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDB21527FC9B6F09BC7BB132750502BD3A956B226);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFAA454C5551F0BFB4AAE794589CC8CBD44FA5A4C);
		s_Il2CppMethodInitialized = true;
	}
	bool V_0 = false;
	CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	bool V_2 = false;
	bool V_3 = false;
	bool V_4 = false;
	bool V_5 = false;
	{
		// if (Num / 100 == 1)
		int32_t L_0 = ___Num0;
		V_0 = (bool)((((int32_t)((int32_t)((int32_t)L_0/(int32_t)((int32_t)100)))) == ((int32_t)1))? 1 : 0);
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		// return MakeCharacterLastItem(CharacterItemName.hat, Num);
		int32_t L_2 = ___Num0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_3;
		L_3 = ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C(_stringLiteralFAA454C5551F0BFB4AAE794589CC8CBD44FA5A4C, L_2, /*hidden argument*/NULL);
		V_1 = L_3;
		goto IL_0096;
	}

IL_001b:
	{
		// else if (Num / 100 == 2)
		int32_t L_4 = ___Num0;
		V_2 = (bool)((((int32_t)((int32_t)((int32_t)L_4/(int32_t)((int32_t)100)))) == ((int32_t)2))? 1 : 0);
		bool L_5 = V_2;
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		// return MakeCharacterLastItem(CharacterItemName.shirt, Num);
		int32_t L_6 = ___Num0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_7;
		L_7 = ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C(_stringLiteral227DA7A8EA45379E2CD7C2CE4658EBC0AC5EC9BE, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		goto IL_0096;
	}

IL_0035:
	{
		// else if (Num / 100 == 3)
		int32_t L_8 = ___Num0;
		V_3 = (bool)((((int32_t)((int32_t)((int32_t)L_8/(int32_t)((int32_t)100)))) == ((int32_t)3))? 1 : 0);
		bool L_9 = V_3;
		if (!L_9)
		{
			goto IL_004f;
		}
	}
	{
		// return MakeCharacterLastItem(CharacterItemName.pants, Num);
		int32_t L_10 = ___Num0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_11;
		L_11 = ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C(_stringLiteral9B4EB089C2114281619D0CCE8FC65CE8F7A6C788, L_10, /*hidden argument*/NULL);
		V_1 = L_11;
		goto IL_0096;
	}

IL_004f:
	{
		// else if (Num / 100 == 4)
		int32_t L_12 = ___Num0;
		V_4 = (bool)((((int32_t)((int32_t)((int32_t)L_12/(int32_t)((int32_t)100)))) == ((int32_t)4))? 1 : 0);
		bool L_13 = V_4;
		if (!L_13)
		{
			goto IL_006b;
		}
	}
	{
		// return MakeCharacterLastItem(CharacterItemName.bag, Num);
		int32_t L_14 = ___Num0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_15;
		L_15 = ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C(_stringLiteral50863FA6986F7E76E712DFF2F2A2AD0E3E7AE14D, L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		goto IL_0096;
	}

IL_006b:
	{
		// else if (Num / 100 == 5)
		int32_t L_16 = ___Num0;
		V_5 = (bool)((((int32_t)((int32_t)((int32_t)L_16/(int32_t)((int32_t)100)))) == ((int32_t)5))? 1 : 0);
		bool L_17 = V_5;
		if (!L_17)
		{
			goto IL_0087;
		}
	}
	{
		// return MakeCharacterLastItem(CharacterItemName.accessories, Num);
		int32_t L_18 = ___Num0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_19;
		L_19 = ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C(_stringLiteralDB21527FC9B6F09BC7BB132750502BD3A956B226, L_18, /*hidden argument*/NULL);
		V_1 = L_19;
		goto IL_0096;
	}

IL_0087:
	{
		// return MakeCharacterLastItem(CharacterItemName.shoe, Num);
		int32_t L_20 = ___Num0;
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_21;
		L_21 = ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C(_stringLiteral2A557BCDD1470D7822A2296783B75E92CBDCB8A3, L_20, /*hidden argument*/NULL);
		V_1 = L_21;
		goto IL_0096;
	}

IL_0096:
	{
		// }
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_22 = V_1;
		return L_22;
	}
}
// CharacterLastItem ExtensionCusTomsCharacter::MakeCharacterLastItem(System.String,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  ExtensionCusTomsCharacter_MakeCharacterLastItem_m0831A1D0B094491E4B19CEC9426F73345DE02E6C (String_t* ___itemName0, int32_t ___numberId1, const RuntimeMethod* method)
{
	CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  V_1;
	memset((&V_1), 0, sizeof(V_1));
	{
		// return new CharacterLastItem {
		//     itemName = itemName,
		//     isActive = numberId % 100 == 1
		// };
		il2cpp_codegen_initobj((&V_0), sizeof(CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E ));
		String_t* L_0 = ___itemName0;
		(&V_0)->set_itemName_0(L_0);
		int32_t L_1 = ___numberId1;
		(&V_0)->set_isActive_1((bool)((((int32_t)((int32_t)((int32_t)L_1%(int32_t)((int32_t)100)))) == ((int32_t)1))? 1 : 0));
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_2 = V_0;
		V_1 = L_2;
		goto IL_0023;
	}

IL_0023:
	{
		// }
		CharacterLastItem_t942330195BD2CAFFB7FC1D7941BAE9AF76AAEE9E  L_3 = V_1;
		return L_3;
	}
}
// System.Int32 ExtensionCusTomsCharacter::ToInt(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t ExtensionCusTomsCharacter_ToInt_m2554F281D955C23A229781EBF24BFEB2410F3FDA (bool ___value0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t G_B3_0 = 0;
	{
		// return value ? 1 : 0;
		bool L_0 = ___value0;
		if (L_0)
		{
			goto IL_0007;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0008;
	}

IL_0007:
	{
		G_B3_0 = 1;
	}

IL_0008:
	{
		V_0 = G_B3_0;
		goto IL_000b;
	}

IL_000b:
	{
		// }
		int32_t L_1 = V_0;
		return L_1;
	}
}
// System.Void ExtensionCusTomsCharacter::ResetPositionCharacter(UnityEngine.Transform,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_ResetPositionCharacter_mF8AA537F5FF98FC57700354E2F3DABFDA635DB9B (Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___transform0, float ___x1, const RuntimeMethod* method)
{
	{
		// transform.position = new Vector3(x, 0, 0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_0 = ___transform0;
		float L_1 = ___x1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2;
		memset((&L_2), 0, sizeof(L_2));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_2), L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_mB169E52D57EEAC1E3F22C5395968714E4F00AC91(L_0, L_2, /*hidden argument*/NULL);
		// transform.rotation = new Quaternion(0, 180, 0, 0);
		Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * L_3 = ___transform0;
		Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  L_4;
		memset((&L_4), 0, sizeof(L_4));
		Quaternion__ctor_m564FA9302F5B9DA8BAB97B0A2D86FFE83ACAA421((&L_4), (0.0f), (180.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m1B5F3D4CE984AB31254615C9C71B0E54978583B4(L_3, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ExtensionCusTomsCharacter::StartScene(CusTomsCharacter)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExtensionCusTomsCharacter_StartScene_mC0988804556E30B98C5D352E867E7F601F3E892B (CusTomsCharacter_t5622F0FF4B482BD5C75A923DFF721AAD0B3992C4 * ___customs0, const RuntimeMethod* method)
{
	{
		// NativeAPI.StartScene();
		NativeAPI_StartScene_m213C8305BFD872BC5ECB55E49ACF01DA1F88C15E(/*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ModelDetails::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModelDetails_Start_m6FB658FB5B4FD02CCF117C740F7120E0267F221D (ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ModelDetails::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModelDetails_Update_mCB2B959309C64D086C8B486942DA9A29BF0662F0 (ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * __this, const RuntimeMethod* method)
{
	{
		// }
		return;
	}
}
// System.Void ModelDetails::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ModelDetails__ctor_m118EF6DBD042861F08936137E6FD167991F85415 (ModelDetails_t5B9240765DF6965784C8E87C4CA131D83F2E01E6 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UISpriteSwap::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISpriteSwap_Start_mFBECA1CCCC144E1C13A7BF1A863DDD950D733952 (UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// ButtonUI = gameObject.GetComponent<Button>();
		GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * L_0;
		L_0 = Component_get_gameObject_m55DC35B149AFB9157582755383BA954655FE0C5B(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_1;
		L_1 = GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8(L_0, /*hidden argument*/GameObject_GetComponent_TisButton_tA893FC15AB26E1439AC25BDCA7079530587BB65D_mC89B59084AF54D6861DE55F9F1FC4226E4F616A8_RuntimeMethod_var);
		__this->set_ButtonUI_7(L_1);
		// }
		return;
	}
}
// System.Void UISpriteSwap::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISpriteSwap_Update_mF5A7E5C1898FFC2D1EC55D6E778F2ED5EFD0EB17 (UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883 * __this, const RuntimeMethod* method)
{
	bool V_0 = false;
	{
		// if (Ready)
		bool L_0 = __this->get_Ready_6();
		V_0 = L_0;
		bool L_1 = V_0;
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		// ButtonUI.image.sprite = SelectionImage;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_2 = __this->get_ButtonUI_7();
		NullCheck(L_2);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_3;
		L_3 = Selectable_get_image_mAB45C107C7C858ECBEFFFF540B8C69746BB6C6FE(L_2, /*hidden argument*/NULL);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_4 = __this->get_SelectionImage_5();
		NullCheck(L_3);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_3, L_4, /*hidden argument*/NULL);
		goto IL_003f;
	}

IL_0026:
	{
		// ButtonUI.image.sprite = MainImage;
		Button_tA893FC15AB26E1439AC25BDCA7079530587BB65D * L_5 = __this->get_ButtonUI_7();
		NullCheck(L_5);
		Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * L_6;
		L_6 = Selectable_get_image_mAB45C107C7C858ECBEFFFF540B8C69746BB6C6FE(L_5, /*hidden argument*/NULL);
		Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * L_7 = __this->get_MainImage_4();
		NullCheck(L_6);
		Image_set_sprite_m55C50F18ABA0A98E926FC777F7D07FA18A7D04E4(L_6, L_7, /*hidden argument*/NULL);
	}

IL_003f:
	{
		// }
		return;
	}
}
// System.Void UISpriteSwap::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UISpriteSwap__ctor_mB92DDC7D233A7F19CAE938F736DC2DD1CC16E9BB (UISpriteSwap_t5EA17DB79675CB914B40809D2CEEC6E718DA3883 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void ExtensionCusTomsCharacter/NativeAPI::CallAndroidMethod(System.String,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_CallAndroidMethod_m309542DA3F713FB85889EFEFF13A15580E579A83 (String_t* ___methodName0, RuntimeObject * ___str1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_mC84C97A7EC20ED712D21107C9FA32E0785021153_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9);
		s_Il2CppMethodInitialized = true;
	}
	AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * V_0 = NULL;
	AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * V_1 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// using var clsUnityPlayer = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_0 = (AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 *)il2cpp_codegen_object_new(AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4_il2cpp_TypeInfo_var);
		AndroidJavaClass__ctor_mEFF9F51871F231955D97DABDE9AB4A6B4EDA5541(L_0, _stringLiteral4D613657609485AE586A3379BA0E3FC13C1E1078, /*hidden argument*/NULL);
		V_0 = L_0;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			// using var objActivity = clsUnityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_1 = V_0;
			NullCheck(L_1);
			AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_2;
			L_2 = AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_mC84C97A7EC20ED712D21107C9FA32E0785021153(L_1, _stringLiteralFB4AE4F77150C3A8E8E4F8B23E734E0C7277B7D9, /*hidden argument*/AndroidJavaObject_GetStatic_TisAndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E_mC84C97A7EC20ED712D21107C9FA32E0785021153_RuntimeMethod_var);
			V_1 = L_2;
		}

IL_0018:
		try
		{ // begin try (depth: 2)
			// objActivity.Call(methodName, str);
			AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_3 = V_1;
			String_t* L_4 = ___methodName0;
			ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_5 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)SZArrayNew(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE_il2cpp_TypeInfo_var, (uint32_t)1);
			ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_6 = L_5;
			RuntimeObject * L_7 = ___str1;
			NullCheck(L_6);
			ArrayElementTypeCheck (L_6, L_7);
			(L_6)->SetAt(static_cast<il2cpp_array_size_t>(0), (RuntimeObject *)L_7);
			NullCheck(L_3);
			AndroidJavaObject_Call_mBB226DA52CE5A2FCD9A2D42BC7FB4272E094B76D(L_3, L_4, L_6, /*hidden argument*/NULL);
			// }
			IL2CPP_LEAVE(0x42, FINALLY_002c);
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_002c;
		}

FINALLY_002c:
		{ // begin finally (depth: 2)
			{
				AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_8 = V_1;
				if (!L_8)
				{
					goto IL_0036;
				}
			}

IL_002f:
			{
				AndroidJavaObject_t10188D5695DCD09C9F621B44B0A8C93A2281236E * L_9 = V_1;
				NullCheck(L_9);
				InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_9);
			}

IL_0036:
			{
				IL2CPP_END_FINALLY(44)
			}
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(44)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_END_CLEANUP(0x42, FINALLY_0037);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0037;
	}

FINALLY_0037:
	{ // begin finally (depth: 1)
		{
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_10 = V_0;
			if (!L_10)
			{
				goto IL_0041;
			}
		}

IL_003a:
		{
			AndroidJavaClass_t52E934B16476D72AA6E4B248F989F2F825EB62D4 * L_11 = V_0;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_11);
		}

IL_0041:
		{
			IL2CPP_END_FINALLY(55)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(55)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x42, IL_0042)
	}

IL_0042:
	{
		// }
		return;
	}
}
// System.Void ExtensionCusTomsCharacter/NativeAPI::SendToNative(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_SendToNative_mE1A3D6410681C7761C6F163B61D1B731995B79F1 (String_t* ___message0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDDE2976751DA4F0E8E52A81CE7C26335F3B9304D);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CallAndroidMethod("sendMessageToMobileApp", message);
		String_t* L_0 = ___message0;
		NativeAPI_CallAndroidMethod_m309542DA3F713FB85889EFEFF13A15580E579A83(_stringLiteralDDE2976751DA4F0E8E52A81CE7C26335F3B9304D, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ExtensionCusTomsCharacter/NativeAPI::LastSelectItem(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_LastSelectItem_mE97F9CE03B9769E54869571EC1AD9A8CAEAC6B68 (String_t* ___characterLastItem0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB8A41C05229F632972A5E5BD30A0C7CD675CD7F1);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CallAndroidMethod("lastSelectItem", characterLastItem);
		String_t* L_0 = ___characterLastItem0;
		NativeAPI_CallAndroidMethod_m309542DA3F713FB85889EFEFF13A15580E579A83(_stringLiteralB8A41C05229F632972A5E5BD30A0C7CD675CD7F1, L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ExtensionCusTomsCharacter/NativeAPI::StartScene()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI_StartScene_m213C8305BFD872BC5ECB55E49ACF01DA1F88C15E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralDE8D19E423C142F62412E91C74431E74DF9816EB);
		s_Il2CppMethodInitialized = true;
	}
	{
		// CallAndroidMethod("startScene", "");
		NativeAPI_CallAndroidMethod_m309542DA3F713FB85889EFEFF13A15580E579A83(_stringLiteralDE8D19E423C142F62412E91C74431E74DF9816EB, _stringLiteralDA39A3EE5E6B4B0D3255BFEF95601890AFD80709, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void ExtensionCusTomsCharacter/NativeAPI::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NativeAPI__ctor_m93471F43776EABE22E0746CF1E98CD3ECD13E0ED (NativeAPI_tF772D52CA80B786271D41D2D49DA78F644213651 * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Subtraction_m2725C96965D5C0B1F9715797E51762B13A5FED58_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___b1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_2 = ___b1;
		float L_3 = L_2.get_x_2();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_4 = ___a0;
		float L_5 = L_4.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___b1;
		float L_7 = L_6.get_y_3();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_8 = ___a0;
		float L_9 = L_8.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = ___b1;
		float L_11 = L_10.get_z_4();
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_12;
		memset((&L_12), 0, sizeof(L_12));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_12), ((float)il2cpp_codegen_subtract((float)L_1, (float)L_3)), ((float)il2cpp_codegen_subtract((float)L_5, (float)L_7)), ((float)il2cpp_codegen_subtract((float)L_9, (float)L_11)), /*hidden argument*/NULL);
		V_0 = L_12;
		goto IL_0030;
	}

IL_0030:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_13 = V_0;
		return L_13;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  Vector3_op_Multiply_m9EA3D18290418D7B410C7D11C4788C13BFD2C30A_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___a0, float ___d1, const RuntimeMethod* method)
{
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  V_0;
	memset((&V_0), 0, sizeof(V_0));
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_0 = ___a0;
		float L_1 = L_0.get_x_2();
		float L_2 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_3 = ___a0;
		float L_4 = L_3.get_y_3();
		float L_5 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_6 = ___a0;
		float L_7 = L_6.get_z_4();
		float L_8 = ___d1;
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_9;
		memset((&L_9), 0, sizeof(L_9));
		Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline((&L_9), ((float)il2cpp_codegen_multiply((float)L_1, (float)L_2)), ((float)il2cpp_codegen_multiply((float)L_4, (float)L_5)), ((float)il2cpp_codegen_multiply((float)L_7, (float)L_8)), /*hidden argument*/NULL);
		V_0 = L_9;
		goto IL_0021;
	}

IL_0021:
	{
		Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  L_10 = V_0;
		return L_10;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void Vector3__ctor_m57495F692C6CE1CEF278CAD9A98221165D37E636_inline (Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * __this, float ___x0, float ___y1, float ___z2, const RuntimeMethod* method)
{
	{
		float L_0 = ___x0;
		__this->set_x_2(L_0);
		float L_1 = ___y1;
		__this->set_y_3(L_1);
		float L_2 = ___z2;
		__this->set_z_4(L_2);
		return;
	}
}
